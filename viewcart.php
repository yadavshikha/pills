<!doctype html>
<html lang="en">



<body id="cart" class="lang-en country-us currency-usd layout-full-width page-cart tax-display-disabled fullwidth">


    <?php
include ('partials/head.php');
?>

    <main>


        <?php
include ('partials/header.php');
?>



        <aside id="notifications">
            <div class="container">



            </div>
        </aside>

        <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">
                <div id="content-wrapper">
                    <section id="main">
                        <div class="cart-grid row">
                            <!-- Left Block: cart product informations & shpping -->
                            <div class="cart-grid-body col-xs-12 col-lg-8">
                                <!-- cart products detailed -->
                                <div class="card cart-container">
                                    <div class="card-block">
                                        <h1 class="h1">Shopping Cart</h1>
                                    </div>
                                    <hr>

                                    <div class="cart-overview js-cart"
                                        data-refresh-url="https://demo.fieldthemes.com/ps_medicine/home1/en/cart?ajax=1&action=refresh">
                                        <ul class="cart-items">
                                            <li class="cart-item">
                                                <div class="product-line-grid">
                                                    <!--  product left content: image-->
                                                    <div class="product-line-grid-left col-md-3 col-xs-4">
                                                        <span class="product-image media-middle">
                                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/80-cart_default/aliquam-tincidunt-mauris.jpg"
                                                                alt="Aliquam tincidunt mauris.">
                                                        </span>
                                                    </div>

                                                    <!--  product left body: description -->
                                                    <div class="product-line-grid-body col-md-4 col-xs-8">
                                                        <div class="product-line-info">
                                                            <a class="label" href="">Aliquam
                                                                tincidunt mauris.</a>
                                                        </div>

                                                        <div class="product-line-info">
                                                            <span class="value">$32.40</span>
                                                        </div>

                                                        <br />


                                                    </div>

                                                    <!--  product left body: description -->
                                                    <div
                                                        class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-4 hidden-md-up"></div>
                                                            <div class="col-md-10 col-xs-6">
                                                                <div class="row">
                                                                    <!-- <div class="col-md-6 col-xs-6 qty"> -->
                                                                    <!-- <input class="cart-qty item-count" type="text" name="product[3797]" value="1"> -->
                                                                    <!-- <input class="js-cart-line-product-quantity"
                                                                            data-down-url="https://demo.fieldthemes.com/ps_medicine/home1/en/cart?update=1&amp;id_product=16&amp;id_product_attribute=0&amp;token=2a1b67dd8a783e9adf2157fbebcfa99f&amp;op=down"
                                                                            data-up-url="https://demo.fieldthemes.com/ps_medicine/home1/en/cart?update=1&amp;id_product=16&amp;id_product_attribute=0&amp;token=2a1b67dd8a783e9adf2157fbebcfa99f&amp;op=up"
                                                                            data-update-url="https://demo.fieldthemes.com/ps_medicine/home1/en/cart?update=1&amp;id_product=16&amp;id_product_attribute=0&amp;token=2a1b67dd8a783e9adf2157fbebcfa99f"
                                                                            data-product-id="16" type="text" value="2"
                                                                            name="product-quantity-spin" min="1" /> -->
                                                                    <!-- </div> -->
                                                                    <div class="col-md-6 col-xs-6">
                                                                        <div class="d-flex justify-content-between">

                                                                            <div
                                                                                class="input-group w-auto justify-content-end align-items-center">
                                                                                <input type="button" value="-"
                                                                                    class="button-minus border rounded-circle  icon-shape icon-sm mx-1 "
                                                                                    data-field="quantity">
                                                                                <input type="" step="1" max="10"
                                                                                    value="1" name="quantity"
                                                                                    class="quantity-field border-0 text-center w-25">
                                                                                <input type="button" value="+"
                                                                                    class="button-plus border rounded-circle icon-shape icon-sm "
                                                                                    data-field="quantity">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-xs-2 price">
                                                                        <span class="product-price">
                                                                            <strong>
                                                                                $64.80
                                                                            </strong>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-xs-2 text-xs-right">
                                                                <div class="cart-line-product-actions ">
                                                                    <a class="remove-from-cart" rel="nofollow"
                                                                        href="https://demo.fieldthemes.com/ps_medicine/home1/en/cart?delete=1&amp;id_product=16&amp;id_product_attribute=0&amp;token=2a1b67dd8a783e9adf2157fbebcfa99f"
                                                                        data-link-action="delete-from-cart"
                                                                        data-id-product="16"
                                                                        data-id-product-attribute="0"
                                                                        data-id-customization="">
                                                                        <i
                                                                            class="material-icons pull-xs-left">delete</i>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <div class="cart-cards">
                                    <img src="img/1383319745_mastercard.png" class="card" align="absmiddle"
                                        alt="MasterCard">

                                    <img src="img/1383319681_visa.png" class="card" align="absmiddle" alt="Visa">


                                    <img src="img/1383319807_amex.png" class="card" align="absmiddle" alt="AmEx">


                                    <img src="img/i_bitcoin.png" alt="Bitcoin" align="absmiddle">

                                </div>

                               
                                <div class="bonusesList">
                                    <ul>
                                        <li class="active"><img src="img/list-active.gif"> Free pills for every order
                                        </li>
                                        <li class="notactive"> <img src="img/list-notactive.gif">Free AirMail Delivery.
                                            <small>For orders more than
                                                $200.00</small></li>
                                        <li class="notactive"><img src="img/list-notactive.gif"> Free Traceable
                                            Delivery. <small>For orders more than
                                                $300.00</small></li>
                                    </ul>

                                </div>
                                <ul class="cart-types ">
                                    <li>
                                        <div class="cart-types-head">
                                            <span class="radioAreaChecked "></span><input type="radio" name="delivery"
                                                class="" value="air" checked="" style="margin-right: 1rem;6">AirMail
                                            <!-- <label for="cart-types1">AirMail</label> -->
                                            <span class="price">$14.95</span>
                                        </div>
                                        <p>The shipment time varies and it may be for about 2-4 weeks. No online
                                            tracking is available</p>
                                    </li>

                                </ul>
                                <div class="cart-delivery">
                                    <span class="checkboxAreaChecked delivery"></span><input class="" id="cart-delivery"
                                        type="checkbox" name="insurance" value="1" checked=""
                                        style="margin-right: 1rem;">
                                    <label for="cart-delivery"><strong>Delivery Insurance - <span>$6.95</span></strong>
                                        (Guaranteed reshipment if delivery failed)</label>
                                </div>
                                <a class="label" href="index.php">
                                    <i class="material-icons">chevron_left</i>Continue shopping
                                </a>
                                <!-- shipping informations -->
                            </div>

                            <div class="cart-grid-right col-xs-12 col-lg-4">
            
            
                                <div class="card cart-summary">
            
            
            
            
                                    <div class="cart-detailed-totals">
            
                                        <div class="card-block">
                                            <div class="cart-summary-line" id="cart-subtotal-products">
                                                <span class="label js-subtotal">
                                                    2 items
                                                </span>
                                                <span class="value">$64.80</span>
                                            </div>
                                            <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                <span class="label">
                                                    Shipping
                                                </span>
                                                <span class="value">$7.00</span>
                                                <div><small class="value"></small></div>
                                            </div>
                                        </div>
            
            
            
            
                                        <hr>
            
                                        <div class="card-block">
                                            <div class="cart-summary-line cart-total">
                                                <span class="label">Total (tax excl.)</span>
                                                <span class="value">$71.80</span>
                                            </div>
            
                                            <div class="cart-summary-line">
                                                <small class="label">Taxes</small>
                                                <small class="value">$0.00</small>
                                            </div>
                                        </div>
            
                                        <hr>
                                    </div>
            
            
            
                                    <div class="checkout cart-detailed-actions card-block">
                                        <div class="text-xs-center">
                                            <a href="shipping.php" class="btn btn-primary">Checkout</a>
            
                                        </div>
                                    </div>
            
            
                                </div>
            
            
            
                                <div id="block-reassurance">
                                    <ul>
                                        <li>
                                            <div class="block-reassurance-item">
                                                <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png"
                                                    alt="Security policy (edit with module Customer reassurance)">
                                                <span class="h6">Security policy (edit with module Customer
                                                    reassurance)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block-reassurance-item">
                                                <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png"
                                                    alt="Delivery policy (edit with module Customer reassurance)">
                                                <span class="h6">Delivery policy (edit with module Customer
                                                    reassurance)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block-reassurance-item">
                                                <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png"
                                                    alt="Return policy (edit with module Customer reassurance)">
                                                <span class="h6">Return policy (edit with module Customer
                                                    reassurance)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
            
            
            
                            </div>
                        </div>
                </div>

                <!-- Right Block: cart subtotal & cart total -->

            </div>
        </section>

        </div>
        <script>
            function incrementValue(e) {
                e.preventDefault();
                var fieldName = $(e.target).data('field');
                var parent = $(e.target).closest('div');
                var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

                if (!isNaN(currentVal)) {
                    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
                } else {
                    parent.find('input[name=' + fieldName + ']').val(0);
                }
            }

            function decrementValue(e) {
                e.preventDefault();
                var fieldName = $(e.target).data('field');
                var parent = $(e.target).closest('div');
                var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

                if (!isNaN(currentVal) && currentVal > 0) {
                    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
                } else {
                    parent.find('input[name=' + fieldName + ']').val(0);
                }
            }

            $('.input-group').on('click', '.button-plus', function (e) {
                incrementValue(e);
            });

            $('.input-group').on('click', '.button-minus', function (e) {
                decrementValue(e);
            });
        </script>





        </div>

        </section>

        <?php
 include ('partials/footer.php');
?>

    </main>

    <!-- <script type="text/javascript"
        src="https://demo.fieldthemes.com/ps_medicine/home1/themes/medicine_home1/assets/cache/bottom-1d7c42.js">
    </script> -->
    <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>




</body>

</html>