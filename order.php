<!doctype html>
<html lang="en">


<?php
include ('partials/head.php');
?>
<body id="checkout" class="lang-en country-us currency-usd layout-full-width page-order tax-display-disabled fullwidth">


    
    <main>

    <?php
include ('partials/header.php');
?>
       
        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <!--END SLIDER SHOW -->


        <aside id="notifications">
            <div class="container">



            </div>
        </aside>

        <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">
                <section id="content">
                    <div class="row">
                        <div class="col-md-8">
                            <section id="checkout-personal-information-step" class="checkout-step -reachable -complete">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">1</span>
                                    Personal Information
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>

                                <div class="content">


                                    <p class="identity">

                                        Connected as <a
                                            href='https://demo.fieldthemes.com/ps_medicine/home1/en/identity'>dsaa
                                            asdsad</a>.
                                    </p>
                                    <p>

                                        Not you? <a
                                            href='https://demo.fieldthemes.com/ps_medicine/home1/en/?mylogout='>Log
                                            out</a>
                                    </p>
                                    <p><small>If you sign out now, your cart will be emptied.</small></p>


                                </div>
                            </section>

                            <section id="checkout-addresses-step"
                                class="checkout-step -current -reachable js-current-step">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">2</span>
                                    Addresses
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>

                                <div class="content">

                                    <div class="js-address-form">
                                        <form method="POST"
                                            action=""
                                            data-refresh-url="https://demo.fieldthemes.com/ps_medicine/home1/en/order?ajax=1&action=addressForm">
                                            <p>
                                                The selected address will be used both as your personal address (for
                                                invoice) and as your delivery address.
                                            </p>
                                            <div id="delivery-address">
                                                <div class="js-address-form">
                                                    <form method="POST"
                                                        action="https://demo.fieldthemes.com/ps_medicine/home1/en/address?id_address=0"
                                                        data-id-address="0"
                                                        data-refresh-url="https://demo.fieldthemes.com/ps_medicine/home1/en/address?ajax=1&action=addressForm">
                                                        <section class="form-fields">
                                                            <input type="hidden" name="id_address" value="">
                                                            <input type="hidden" name="id_customer" value="">
                                                            <input type="hidden" name="back" value="">
                                                            <input type="hidden" name="token"
                                                                value="fdd6f38386170833744426278d07365e">
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    First name
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="firstname"
                                                                        type="text" value="dsaa" maxlength="32"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Last name
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="lastname"
                                                                        type="text" value="asdsad" maxlength="32"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label">
                                                                    Company
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="company"
                                                                        type="text" value="" maxlength="64">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                    Optional
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Address
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="address1"
                                                                        type="text" value="" maxlength="128" required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label">
                                                                    Address Complement
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="address2"
                                                                        type="text" value="" maxlength="128">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                    Optional
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    City
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="city" type="text"
                                                                        value="" maxlength="64" required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    State
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control form-control-select"
                                                                        name="id_state" required>
                                                                        <option value disabled selected>-- please choose
                                                                            --</option>
                                                                        <option value="1">AA</option>
                                                                        <option value="2">AE</option>
                                                                        <option value="3">AP</option>
                                                                        <option value="4">Alabama</option>
                                                                        <option value="5">Alaska</option>
                                                                        <option value="6">Arizona</option>
                                                                        <option value="7">Arkansas</option>
                                                                        <option value="8">California</option>
                                                                        <option value="9">Colorado</option>
                                                                        <option value="10">Connecticut</option>
                                                                        <option value="11">Delaware</option>
                                                                        <option value="12">Florida</option>
                                                                        <option value="13">Georgia</option>
                                                                        <option value="14">Hawaii</option>
                                                                        <option value="15">Idaho</option>
                                                                        <option value="16">Illinois</option>
                                                                        <option value="17">Indiana</option>
                                                                        <option value="18">Iowa</option>
                                                                        <option value="19">Kansas</option>
                                                                        <option value="20">Kentucky</option>
                                                                        <option value="21">Louisiana</option>
                                                                        <option value="22">Maine</option>
                                                                        <option value="23">Maryland</option>
                                                                        <option value="24">Massachusetts</option>
                                                                        <option value="25">Michigan</option>
                                                                        <option value="26">Minnesota</option>
                                                                        <option value="27">Mississippi</option>
                                                                        <option value="28">Missouri</option>
                                                                        <option value="29">Montana</option>
                                                                        <option value="30">Nebraska</option>
                                                                        <option value="31">Nevada</option>
                                                                        <option value="32">New Hampshire</option>
                                                                        <option value="33">New Jersey</option>
                                                                        <option value="34">New Mexico</option>
                                                                        <option value="35">New York</option>
                                                                        <option value="36">North Carolina</option>
                                                                        <option value="37">North Dakota</option>
                                                                        <option value="38">Ohio</option>
                                                                        <option value="39">Oklahoma</option>
                                                                        <option value="40">Oregon</option>
                                                                        <option value="41">Pennsylvania</option>
                                                                        <option value="42">Rhode Island</option>
                                                                        <option value="43">South Carolina</option>
                                                                        <option value="44">South Dakota</option>
                                                                        <option value="45">Tennessee</option>
                                                                        <option value="46">Texas</option>
                                                                        <option value="47">Utah</option>
                                                                        <option value="48">Vermont</option>
                                                                        <option value="49">Virginia</option>
                                                                        <option value="50">Washington</option>
                                                                        <option value="51">West Virginia</option>
                                                                        <option value="52">Wisconsin</option>
                                                                        <option value="53">Wyoming</option>
                                                                        <option value="54">Puerto Rico</option>
                                                                        <option value="55">US Virgin Islands</option>
                                                                        <option value="56">District of Columbia</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Zip/Postal Code
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="postcode"
                                                                        type="text" value="" maxlength="12" required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Country
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <select
                                                                        class="form-control form-control-select js-country"
                                                                        name="id_country" required>
                                                                        <option value disabled selected>-- please choose
                                                                            --</option>
                                                                        <option value="21" selected>United States
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label">
                                                                    Phone
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="phone" type="text"
                                                                        value="" maxlength="32">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                    Optional
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="saveAddress" value="delivery">
                                                            <div class="form-group row">
                                                                <div class="col-md-9 col-md-offset-3">
                                                                    <input name="use_same_address" type="checkbox"
                                                                        value="1" checked>
                                                                    <label>Use this address for invoice too</label>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <footer class="form-footer clearfix">
                                                            <input type="hidden" name="submitAddress" value="1">

                                                            <form>
                                                                <button type="submit"
                                                                    class="continue btn btn-primary pull-xs-right"
                                                                    name="confirm-addresses" value="1">
                                                                    Continue
                                                                </button>
                                                            </form>

                                                        </footer>
                                                    </form>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                            <section class="checkout-step -unreachable" id="checkout-delivery-step">
                                <h1 class="step-title h3">
                                    <span class="step-number">3</span> Shipping Method
                                </h1>
                            </section>
                            <!-- <section class="checkout-step -unreachable" id="checkout-payment-step">
                                <h1 class="step-title h3">
                                    <span class="step-number">4</span> Payment
                                </h1>
                            </section> -->
                        </div>
                        <!-- <div class="col-md-4">

                            <section id="js-checkout-summary" class="card js-cart"
                                data-refresh-url="https://demo.fieldthemes.com/ps_medicine/home1/en/cart?ajax=1">
                                <div class="card-block">


                                    <div class="cart-summary-products">

                                        <p>5 items</p>

                                        <p>
                                            <a href="#" data-toggle="collapse" data-target="#cart-summary-product-list">
                                                show details
                                            </a>
                                        </p>


                                        <div class="collapse" id="cart-summary-product-list">
                                            <ul class="media-list">
                                                <li class="media">
                                                    <div class="media-left">
                                                        <a href="https://demo.fieldthemes.com/ps_medicine/home1/en/product-categories/19-auctor-ex-id-accumsan.html"
                                                            title="Auctor ex id accumsan">
                                                            <img class="media-object"
                                                                src="https://demo.fieldthemes.com/ps_medicine/home1/83-small_default/auctor-ex-id-accumsan.jpg"
                                                                alt="Auctor ex id accumsan">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="product-name">Auctor ex id accumsan</span>
                                                        <span class="product-quantity">x1</span>
                                                        <span class="product-price pull-xs-right">$40.37</span>

                                                    </div>
                                                </li>
                                                <li class="media">
                                                    <div class="media-left">
                                                        <a href="https://demo.fieldthemes.com/ps_medicine/home1/en/product-categories/20-fusce-porttitor-augue-lectus.html"
                                                            title="Fusce porttitor augue lectus.">
                                                            <img class="media-object"
                                                                src="https://demo.fieldthemes.com/ps_medicine/home1/84-small_default/fusce-porttitor-augue-lectus.jpg"
                                                                alt="Fusce porttitor augue lectus.">
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <span class="product-name">Fusce porttitor augue lectus.</span>
                                                        <span class="product-quantity">x4</span>
                                                        <span class="product-price pull-xs-right">$60.59</span>

                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>



                                    <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products">
                                        <span class="label">Subtotal</span>
                                        <span class="value">$282.73</span>
                                    </div>
                                    <div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping">
                                        <span class="label">Shipping</span>
                                        <span class="value">$7.00</span>
                                    </div>


                                </div>




                                <hr>


                                <div class="card-block cart-summary-totals">


                                    <div class="cart-summary-line cart-total">
                                        <span class="label">Total (tax excl.)</span>
                                        <span class="value">$289.73</span>
                                    </div>



                                    <div class="cart-summary-line">
                                        <span class="label sub">Taxes</span>
                                        <span class="value sub">$0.00</span>
                                    </div>


                                </div>


                            </section>

                            <div id="block-reassurance">
                                <ul>
                                    <li>
                                        <div class="block-reassurance-item">
                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png"
                                                alt="Security policy (edit with module Customer reassurance)">
                                            <span class="h6">Security policy (edit with module Customer
                                                reassurance)</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block-reassurance-item">
                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png"
                                                alt="Delivery policy (edit with module Customer reassurance)">
                                            <span class="h6">Delivery policy (edit with module Customer
                                                reassurance)</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block-reassurance-item">
                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png"
                                                alt="Return policy (edit with module Customer reassurance)">
                                            <span class="h6">Return policy (edit with module Customer
                                                reassurance)</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div> -->
                    </div>
                </section>
            </div>

        </section>

        <?php
include ('partials/footer.php');
?>

    </main>
</body>

</html>