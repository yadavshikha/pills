<!doctype html>
<html lang="en">


<!-- Mirrored from demo.fieldthemes.com/ps_medicine/home1/en/login by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Feb 2022 13:45:02 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<?php
include ('partials/head.php');
?>


<body id="authentication"
    class="lang-en country-us currency-usd layout-full-width page-authentication tax-display-disabled page-customer-account fullwidth">


   

    <main>

    <?php
include ('partials/header.php');
?>
        
        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <!--END SLIDER SHOW -->


        <aside id="notifications">
            <div class="container">



            </div>
        </aside>

        <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">

                <div id="content-wrapper">

                    <section id="main">
                        <h2 style="display:none">.</h2>
                        <header class="page-header">
                            <h1>
                                Log in to your account
                            </h1>
                        </header>
                        <section id="content" class="page-content card card-block">
                            <section class="login-form">
                                <form id="login-form" action=""
                                    method="post">
                                    <section>
                                        <input type="hidden" name="back" value="">
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label required">
                                                Email
                                            </label>
                                            <div class="col-md-6">
                                                <input class="form-control" name="email" type="email" value="" required>
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label required">
                                                Password
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group js-parent-focus">
                                                    <input class="form-control js-child-focus js-visible-password"
                                                        name="password" type="password" value="" pattern=".{5,}"
                                                        required>
                                                    <span class="input-group-btn">
                                                        <button class="btn" type="button" data-action="show-password"
                                                            data-text-show="Show" data-text-hide="Hide">
                                                            Show
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="forgot-password">
                                            <a href="password-recovery.php  " rel="nofollow">
                                                Forgot your password?
                                            </a>
                                        </div>
                                    </section>
                                    <footer class="form-footer text-xs-center clearfix">
                                        <input type="hidden" name="submitLogin" value="1">
                                        <button class="btn btn-primary" data-link-action="sign-in" type="submit"
                                            class="form-control-submit">
                                            Sign in
                                        </button>
                                    </footer>
                                </form>
                            </section>
                            <hr />
                            <div class="no-account">
                                <a href="create-account.php?create_account=1" data-link-action="display-register-form">
                                    No account? Create one here
                                </a>
                            </div>
                        </section>
                        <footer class="page-footer">
                            <!-- Footer content -->
                        </footer>
                    </section>
                </div>
            </div>
        </section>
        <?php
include ('partials/footer.php');
?>
    </main>
    <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>
</body>




</html>