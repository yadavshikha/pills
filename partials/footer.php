<footer id="footer">
            <div class="Brands-block-slider">
                <!-- <div class="container">
                    <div id="fieldbrandslider" class="block horizontal_mode">
                        <h4 class="title_block title_font"><a class="title_text" href="manufacturers.html">Top
                                Brands</a></h4>
                        <div class="row">
                            <div id="fieldbrandslider-manufacturers" class="grid carousel-grid owl-carousel">
                                <div class="item">
                                    <a class="img-wrapper" href="2_brand1.html" title="brand1">
                                        <img class="img-responsive" src="img/m/2-field_manufacture.jpg"
                                            alt="brand1" />
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="img-wrapper" href="3_brand2.html" title="brand2">
                                        <img class="img-responsive" src="img/m/3-field_manufacture.jpg"
                                            alt="brand2" />
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="img-wrapper" href="4_brand3.html" title="brand3">
                                        <img class="img-responsive" src="img/m/4-field_manufacture.jpg"
                                            alt="brand3" />
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="img-wrapper" href="5_brand4.html" title="brand4">
                                        <img class="img-responsive" src="img/m/5-field_manufacture.jpg"
                                            alt="brand4" />
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="img-wrapper" href="6_brand5.html" title="brand5">
                                        <img class="img-responsive" src="img/m/6-field_manufacture.jpg"
                                            alt="brand5" />
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="img-wrapper" href="7_brand6.html" title="brand6">
                                        <img class="img-responsive" src="img/m/7-field_manufacture.jpg"
                                            alt="brand6" />
                                    </a>
                                </div>
                                <div class="item">
                                    <a class="img-wrapper" href="8_brand7.html" title="brand7">
                                        <img class="img-responsive" src="img/m/8-field_manufacture.jpg"
                                            alt="brand7" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div> -->
            </div>
            <div class="footer-container">
                <div class="footer-top-before">
                    <div class="container">
                        <div class="row">

                        </div>
                    </div>
                </div>
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-3 links contact_ft">
                                <h3>CONTACT US</h3>
                                <div class="title" data-target="#footer_sub_menu_col_1" data-toggle="collapse">
                                    <div class="navbar-toggler collapse-icons hidden-md-up">
                                        <div class="fa fa-plus add"></div>
                                        <div class="fa fa-minus remove"></div>
                                    </div>
                                </div>
                                <ul id="footer_sub_menu_col_1" class="collapse">
                                    <li>
                                        <div class="fa fa-map-marker"></div>
                                        <a href="#"><span>1234 Heaven Stress, Beverly Hill<br />OldYork- United State of
                                                Lorem</span></a>
                                    </li>
                                    <li>
                                        <div class="fa fa-phone"></div>
                                        <span><strong>US TollFree : +1-845-393-0026</strong><br /><strong>Europe : +44-117-318-5431</strong></span>
                                    </li>
                                    <li>
                                        <div class="fa fa-envelope"></div>
                                        <a href="#">support1@demo.com<br />support2@demo.com</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 links bullet">
                                <h3>PAYMENT & SHIPPING</h3>
                                <div class="title" data-target="#footer_sub_menu_col_2" data-toggle="collapse">
                                    <div class="navbar-toggler collapse-icons hidden-md-up">
                                        <div class="fa fa-plus add"></div>
                                        <div class="fa fa-minus remove"></div>
                                    </div>
                                </div>
                                <ul id="footer_sub_menu_col_2" class="collapse">
                                    <li><a href="#" title="">Term of Use</a></li>
                                    <li><a href="#" title="">Payment Methods</a></li>
                                    <li><a href="#" title="">Shipping Guide</a></li>
                                    <li><a href="#" title="">Locations We Ship To</a></li>
                                    <li><a href="#" title="">Estimated Delivery Time</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 links bullet">
                                <h3>PRODUCTS</h3>
                                <div class="title" data-target="#footer_sub_menu_col_2" data-toggle="collapse">
                                    <div class="navbar-toggler collapse-icons hidden-md-up">
                                        <div class="fa fa-plus add"></div>
                                        <div class="fa fa-minus remove"></div>
                                    </div>
                                </div>
                                <ul id="footer_sub_menu_col_2" class="collapse">
                                    <li><a href="medicine-category.php" title=""> Allergies</a></li>
                                    <li><a href="medicine-category.php" title="">Anti Fungal</a></li>
                                    <li><a href="medicine-category.php" title="">Antibiotics</a></li>
                                    <li><a href="medicine-category.php" title="">Anxiety</a></li>
                                    <li><a href="medicine-category.php" title=""> Blood Pressure</a></li>
                                </ul>
                            </div>
                            <!-- <div class="col-xs-12 col-sm-6 col-md-3 links product">
                                <h3><span style="color: #ffffff;">PRODUCT TAGS</span></h3>
                                <div class="title" data-target="#footer_sub_menu_col_3" data-toggle="collapse">
                                    <div class="navbar-toggler collapse-icons hidden-md-up">
                                        <div class="fa fa-plus add"></div>
                                        <div class="fa fa-minus remove"></div>
                                    </div>
                                </div>
                                <ul id="footer_sub_menu_col_3" class="collapse producttags" style="float: left;">
                                    <li><a href="#" title="">Women</a></li>
                                    <li><a href="#" title="">Man</a></li>
                                    <li><a href="#" title="">Fashion & Beauty</a></li>
                                    <li><a href="#" title="">Mobile</a></li>
                                    <li><a href="#" title="">Tablet</a></li>
                                    <li><a href="#" title="">Electronic</a></li>
                                    <li><a href="#" title="">Shopping</a></li>
                                    <li><a href="#" title="">Furniture</a></li>
                                    <li><a href="#" title="">Laptop</a></li>
                                    <li><a href="#" title="">Smart phone</a></li>
                                    <li><a href="#" title="">Gifts</a></li>
                                    <li><a href="#" title="">Electronic</a></li>
                                    <li><a href="#" title="">Fashion & Beauty</a></li>
                                    <li><a href="#" title="">Shopping</a></li>
                                </ul>
                            </div> -->
                            <div class="col-xs-12 col-sm-6 col-md-3 links about_ft">
                                <div class="opening">
                                    <h3>OPENING TIME</h3>
                                    <div class="title" data-target="#footer_sub_menu_col_4" data-toggle="collapse">
                                        <div class="navbar-toggler collapse-icons hidden-md-up">
                                            <div class="fa fa-plus add"></div>
                                            <div class="fa fa-minus remove"></div>
                                        </div>
                                    </div>
                                    <ul id="footer_sub_menu_col_4" class="collapse">
                                        <li>
                                            <p>Monday - Friday........9:00 - 22:00</p>
                                        </li>
                                        <li>
                                            <p>Saturday........10:00 - 24:00</p>
                                        </li>
                                        <li>
                                            <p>Sunday........12:00 - 24:00</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="payment">
                                    <h3>PAYMENT METHODS</h3>
                                    <ul>
                                        <li><img class="img-responsive"
                                                src="modules/fieldstaticfooter/images/payment_footer.png" alt="" />
                                        </li>
                                    </ul>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="footer-address fieldFullWidth">
                                <div class="container">
                                    <p>Copyright © 2017 <a href="http://fieldthemes.com/">Fieldthemes.com</a>. All
                                        Rights Reserved.</p>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
            <div id="back-top"><a href="javascript:void(0)" class="mypresta_scrollup hidden-phone"><i
                        class="fa fa-chevron-up"></i></a></div>
        </footer>
        <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>
        <script type="text/javascript" src="assets/js/bottom-1d7c42.js"></script>
        <!-- <script type="text/javascript" src="../assets/js/bottom-3ef2d4.js"></script> -->

