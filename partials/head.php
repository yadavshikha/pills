<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <title>Medicine home 1</title>
    <meta name="description" content="Shop powered by PrestaShop">
    <meta name="keywords" content="">


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="icon" type="image/vnd.microsoft.icon"
        href="https://demo.fieldthemes.com/ps_medicine/home1/img/favicon.ico?1491894693">
    <link rel="shortcut icon" type="image/x-icon"
        href="https://demo.fieldthemes.com/ps_medicine/home1/img/favicon.ico?1491894693">

     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"> 
     <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/theme-4c962a.css" type="text/css" media="all">
    <link rel="stylesheet" href="assets/css/theme-5f31b3.css" type="text/css" media="all">
    <link rel="stylesheet" href="assets/css/theme-07c24c.css" type="text/css" media="all">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all" >
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">



    <script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript">
        var LANG_RTL = 0;
        var langIso = 'en-us';
        var baseUri = 'https://demo.fieldthemes.com/ps_medicine/home1/';
        var FIELD_enableCountdownTimer = true;
        var FIELD_stickyMenu = true;
        var FIELD_stickySearch = true;
        var FIELD_stickyCart = true;
        var FIELD_mainLayout = 'fullwidth';
        var countdownDay = 'Day';
        var countdownDays = 'Days';
        var countdownHour = 'Hour';
        var countdownHours = 'Hours';
        var countdownMinute = 'Min';
        var countdownMinutes = 'Mins';
        var countdownSecond = 'Sec';
        var countdownSeconds = 'Secs';
    </script>




    <script type="text/javascript">
        var fieldbs_autoscroll = false;
        var fieldbs_maxitem = "6";
        var fieldbs_minitem = "2";
        var fieldbs_navigation = true;
        var fieldbs_pagination = false;
        var fieldbs_pauseonhover = false;
        var prestashop = {
            "currency": {
                "name": "US Dollar",
                "iso_code": "USD",
                "iso_code_num": "840",
                "sign": "$"
            },
            "customer": {
                "lastname": null,
                "firstname": null,
                "email": null,
                "last_passwd_gen": null,
                "birthday": null,
                "newsletter": null,
                "newsletter_date_add": null,
                "ip_registration_newsletter": null,
                "optin": null,
                "website": null,
                "company": null,
                "siret": null,
                "ape": null,
                "outstanding_allow_amount": 0,
                "max_payment_days": 0,
                "note": null,
                "is_guest": 0,
                "id_shop": null,
                "id_shop_group": null,
                "id_default_group": 1,
                "date_add": null,
                "date_upd": null,
                "reset_password_token": null,
                "reset_password_validity": null,
                "id": null,
                "is_logged": false,
                "gender": {
                    "type": null,
                    "name": null,
                    "id": null
                },
                "risk": {
                    "name": null,
                    "color": null,
                    "percent": null,
                    "id": null
                },
                "addresses": []
            },
            "language": {
                "name": "English (English)",
                "iso_code": "en",
                "locale": "en-US",
                "language_code": "en-us",
                "is_rtl": "0",
                "date_format_lite": "m\/d\/Y",
                "date_format_full": "m\/d\/Y H:i:s",
                "id": 1
            },
            "page": {
                "title": "",
                "canonical": null,
                "meta": {
                    "title": "Medicine home 1",
                    "description": "Shop powered by PrestaShop",
                    "keywords": "",
                    "robots": "index"
                },
                "page_name": "index",
                "body_classes": {
                    "lang-en": true,
                    "lang-rtl": false,
                    "country-US": true,
                    "currency-USD": true,
                    "layout-full-width": true,
                    "page-index": true,
                    "tax-display-disabled": true
                },
                "admin_notifications": []
            },
            "shop": {
                "name": "Medicine home 1",
                "email": "demo@fieldthemes.com",
                "registration_number": "",
                "long": false,
                "lat": false,
                "logo": "\/ps_medicine\/home1\/img\/medicinehome1-logo-1491894693.jpg",
                "stores_icon": "\/ps_medicine\/home1\/img\/logo_stores.png",
                "favicon": "\/ps_medicine\/home1\/img\/favicon.ico",
                "favicon_update_time": "1491894693",
                "address": {
                    "formatted": "Medicine home 1<br>United States",
                    "address1": "",
                    "address2": "",
                    "postcode": "",
                    "city": "",
                    "state": null,
                    "country": "United States"
                },
                "phone": "",
                "fax": ""
            },
            "urls": {
                "base_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/",
                "current_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/",
                "shop_domain_url": "https:\/\/demo.fieldthemes.com",
                "img_ps_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/",
                "img_cat_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/c\/",
                "img_lang_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/l\/",
                "img_prod_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/p\/",
                "img_manu_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/m\/",
                "img_sup_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/su\/",
                "img_ship_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/s\/",
                "img_store_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/st\/",
                "img_col_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/img\/co\/",
                "img_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/themes\/medicine_home1\/assets\/img\/",
                "css_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/themes\/medicine_home1\/assets\/css\/",
                "js_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/themes\/medicine_home1\/assets\/js\/",
                "pic_url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/upload\/",
                "pages": {
                    "address": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/address",
                    "addresses": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/addresses",
                    "authentication": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/login",
                    "cart": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/cart",
                    "category": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=category",
                    "cms": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=cms",
                    "contact": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/contact-us",
                    "discount": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/discount",
                    "guest_tracking": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/guest-tracking",
                    "history": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/order-history",
                    "identity": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/identity",
                    "index": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/",
                    "my_account": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/my-account",
                    "order_confirmation": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/order-confirmation",
                    "order_detail": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=order-detail",
                    "order_follow": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/order-follow",
                    "order": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/order",
                    "order_return": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=order-return",
                    "order_slip": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/credit-slip",
                    "pagenotfound": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/page-not-found",
                    "password": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/password-recovery",
                    "pdf_invoice": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=pdf-invoice",
                    "pdf_order_return": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=pdf-order-return",
                    "pdf_order_slip": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=pdf-order-slip",
                    "prices_drop": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/prices-drop",
                    "product": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/index.php?controller=product",
                    "search": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/search",
                    "sitemap": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/sitemap",
                    "stores": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/stores",
                    "supplier": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/supplier",
                    "register": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/login?create_account=1",
                    "order_login": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/order?login=1"
                },
                "theme_assets": "\/ps_medicine\/home1\/themes\/medicine_home1\/assets\/",
                "actions": {
                    "logout": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/?mylogout="
                }
            },
            "configuration": {
                "display_taxes_label": false,
                "low_quantity_threshold": 3,
                "is_b2b": false,
                "is_catalog": false,
                "show_prices": true,
                "opt_in": {
                    "partner": true
                },
                "quantity_discount": {
                    "type": "discount",
                    "label": "Discount"
                },
                "voucher_enabled": 0,
                "return_enabled": 0,
                "number_of_days_for_return": 14
            },
            "field_required": [],
            "breadcrumb": {
                "links": [{
                    "title": "Home",
                    "url": "https:\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/"
                }],
                "count": 1
            },
            "link": {
                "protocol_link": "https:\/\/",
                "protocol_content": "https:\/\/"
            },
            "time": 1645418789,
            "static_token": "9494207df78e080f0efb7bd612000682",
            "token": "1f7addabc9671305f6cf03157e0b9da3"
        };
        var search_url = "index.html\/\/demo.fieldthemes.com\/ps_medicine\/home1\/en\/search";
    </script>







</head>