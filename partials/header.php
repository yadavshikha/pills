<header id="header">

    <div class="header-top">
        <div class="container">
            <div class="logo_header">
                <a href="https://demo.fieldthemes.com/ps_medicine/home1/">
                    <img class="img-responsive" src="img/medicinehome1-logo-1491894693.jpg" alt="Medicine home 1">
                </a>
            </div>
            <div id="sticky_top">
                <!-- block seach mobile -->
                <!-- Block search module TOP -->
                <div id="search_block_top" class="dropdown js-dropdown">
                    <div class="current fa fa-search expand-more" data-toggle="dropdown"></div>
                    <div class="dropdown-menu">
                        <div class="field-search">
                            <form method="get" action="https://demo.fieldthemes.com/ps_medicine/home1/en/search"
                                id="searchbox">
                                <input type="hidden" name="controller" value="search" />
                                <input type="hidden" name="orderby" value="position" />
                                <input type="hidden" name="orderway" value="desc" />
                                <input class="search_query" type="text" id="search_query_top" name="search_query"
                                    value="" placeholder="Search..." />
                            </form>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var $searchWidget = $('#search_widget');
                        var $searchBox = $('#search_query_top');
                        var searchURL = search_url;
                        $.widget('prestashop.psBlockSearchAutocomplete', $.ui.autocomplete, {
                            delay: 0,
                            _renderItem: function (ul, product) {
                                return $("<li>")
                                    .append($("<a>")
                                        .append($("<span class='left-search-ajax'>").html(
                                            '<img src="' + product.images[0].bySize
                                            .small_default.url + '">'))
                                        .append($("<span class='right-search-ajax'>").html(
                                            '<span class="search-name-ajax">' + product.name +
                                            '</span><span class="price-search-ajax">' + (product
                                                .regular_price != product.price ?
                                                '<span class="price-regular-ajax">' + product
                                                .regular_price + '</span>' : '') +
                                            '<span class="price-ajax">' + product.price +
                                            '</span></span>'))
                                    ).appendTo(ul);
                            }
                        });
                        $searchBox.psBlockSearchAutocomplete({
                            delay: 0,
                            source: function (query, response) {
                                $.get(searchURL, {
                                        s: query.term,
                                        category_filter: $("#category_filter").val(),
                                        resultsPerPage: 20
                                    }, null, 'json')
                                    .then(function (resp) {
                                        response(resp.products);
                                    })
                                    .fail(response);
                            },
                            select: function (event, ui) {
                                var url = ui.item.url;
                                window.location.href = url;
                            },
                        });
                    });
                </script>

                <!-- /Block search module TOP -->
                <div id="cart_block_top" class="sticky_top">
                    <div class="blockcart cart-preview  inactive" data-refresh-url="">
                        <div class="click-cart ">
                            <span class="unline_cart">
                                <a href="viewcart.php"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>
                                <!-- <span class="cart-products-count">0</span>
                                <span class="cart-item-top">Item -</span>
                                <span class="cart-total-top">$0.00</span> -->
                            </span>
                            <span class="shopping-cart">
                                <span class="fa fa-shopping-cart">
                                </span>
                                <span class="cart-products-count">0</span>
                            </span>
                        </div>
                        <div class="cart_top_ajax">
                            <div class="card-block-top" style="border-top:none">
                                There are no more items in your cart
                            </div>
                            <div class="card-block-top">
                                <div class="totals-top">
                                    <span class="label-top">Total (tax excl.)</span>
                                    <span class="value-top price">$0.00</span>
                                </div>
                                <div class="totals-top">
                                    <span class="label-top">Taxes</span>
                                    <span class="value-top price">$0.00</span>
                                </div>
                            </div>
                            <div class="card-block-top">
                                <a href="viewcart.php?action=show" class="view-cart">View Cart</a>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $(".click-cart").click(function (e) {
                                    $(".cart_top_ajax").slideToggle();
                                    $(".click-cart").toggleClass('active');

                                    e.stopPropagation();
                                });
                                $("body").click(function (e) {
                                    $(".cart_top_ajax").slideUp();
                                    $(".click-cart").removeClass('active');
                                });
                            });
                        </script>

                    </div>
                </div>
                <ul>
                    <li>
                        <a href="login.php" class=" blockcart  userlog" title="Log in to your customer account" rel="nofollow" >
                            <i class="fa fa-user"></i>
                            <span>Sign in</span>    
                        </a>
                    </li>

                </ul>
            </div>


            <!-- MEGAMENU -->
            <div id="header_menu" class="visible-lg visible-md">
                <div class="container">
                    <div class="row">

                        <div class="container">
                            <div class="logo_header">
                                <a href="https://demo.fieldthemes.com/ps_medicine/home1/">
                                    <img class="img-responsive" src="img/medicinehome1-logo-1491894693.jpg"
                                        alt="Medicine home 1">
                                </a>
                            </div>
                            <nav id="fieldmegamenu-main" class="fieldmegamenu inactive">
                                <ul>
                                    <li class="root root-1 " style=" margin-left: 13rem;">
                                        <div class="root-item no-description">
                                            <a href="index.php">
                                                <div class="title title_font homemag"><span
                                                        class="fa menu-home"></span><span class="title-text">HOME</span>
                                                </div>
                                            </a>
                                        </div>

                                    </li>
                                    <li class="root root-2 ">
                                        <div class="root-item no-description">
                                            <a href="about_us.php">
                                                <div class="title title_font"><span class="title-text">About
                                                        Us</span><span class="icon-has-sub fa fa-angle-down"></span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>



                                    </li>
                                    <li class="root root-3 ">

                                        <div class="root-item no-description">
                                            <a href="#">
                                                <div class="title title_font"><span
                                                        class="title-text">Product</span><span
                                                        class="icon-has-sub fa fa-angle-down"></span></div>
                                            </a>
                                        </div>
                                        <ul class="menu-items col-md-6 col-xs-12">
                                            <li class="menu-item menu-item-121 depth-1 category menucol-1-3  ">
                                                <ul class="submenu submenu-depth-2">
                                                    <li class="menu-item menu-item-122 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Bestsellers">
                                                                Bestsellers
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-123 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Erectile Dysfunction">
                                                                Erectile Dysfunction
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-124 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="ED Sets">
                                                                ED Sets
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Allergies">
                                                                Allergies
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Anti Fungal">
                                                                Anti Fungal
                                                            </a>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Anti Viral">
                                                                Anti Viral
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Antibiotics ">
                                                                Antibiotics
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Anxiety ">
                                                                Anxiety
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Arthritis  ">
                                                                Arthritis
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Asthma">
                                                                Asthma
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item menu-item-140 depth-1 customlink menucol-1-3  ">

                                                <ul class="submenu submenu-depth-2">
                                                    <li class="menu-item menu-item-127 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Birth Control">
                                                                Birth Control
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-129 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Blood Pressure">
                                                                Blood Pressure
                                                            </a>
                                                        </div>
                                                    </li>

                                                    <li class="menu-item menu-item-128 depth-2 category   ">
                                                        <div class="title">

                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Cholesterol Lowering">
                                                                Cholesterol Lowering
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Depression">
                                                                Depression
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Diabetes">
                                                                Diabetes
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Gastrointestinal">
                                                                Gastrointestinal
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Hair Loss">
                                                                Hair Loss
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Heart Disease">
                                                                Heart Disease
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Herbals">
                                                                Herbals
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-130 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Man's Health ">
                                                                Man's Health
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>

                                            <li class="menu-item menu-item-126 depth-1 category menucol-1-3  ">
                                                <ul class="submenu submenu-depth-2">
                                                    <li class="menu-item menu-item-131 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Muscle Relaxant">
                                                                Muscle Relaxant
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-132 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Other">
                                                                Other
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-133 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title=" Pain Relief">
                                                                Pain Relief
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-134 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Skincare">
                                                                Skincare
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-134 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Sleep Aid">
                                                                Sleep Aid
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-134 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Quit Smoking">
                                                                Quit Smoking
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-134 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Weight Loss">
                                                                Weight Loss
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item menu-item-134 depth-2 category   ">
                                                        <div class="title">
                                                            <a class="name_block" href="medicine-category.php"
                                                                title="Woman's Health ">
                                                                Woman's Health
                                                            </a>
                                                        </div>

                                                </ul>
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="root root-4 ">
                                        <div class="root-item no-description">
                                            <a href="Shipping_Policy.php">
                                                <div class="title title_font"><span class="title-text">Shipping
                                                        Policy</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>


                                    <li class="root root-6 " style=" margin-left: 12rem;">
                                        <div class="root-item no-description">
                                            <a href="testimonial.php">
                                                <div class="title title_font "><span
                                                        class="title-text">Testimonial</span></div>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="root root-5 ">
                                        <div class="root-item no-description right">
                                            <a href="contact_us.php">
                                                <div class="title title_font "><span class="title-text">Contact
                                                        Us
                                                    </span><span class="icon-has-sub fa fa-angle-down"></span></div>
                                            </a>
                                        </div>
                                    </li>
                                   

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>


            </div>
</header>
<div id="header_mobile_menu" class="navbar-inactive visible-sm visible-xs">
    <div class="container">
        <div class="fieldmm-nav col-sm-12 col-xs-12">
            <span class="brand">Menu list</span>
            <span id="fieldmm-button"><i class="fa fa-reorder"></i></span>
            <nav id="fieldmegamenu-mobile" class="fieldmegamenu inactive">
                <ul>
                    <li class="root root-1 ">
                        <div class="root-item no-description">
                            <a href="index.php">
                                <div class="title title_font"><span class="fa menu-home"></span><span
                                        class="title-text">HOME</span></div>
                            </a>
                        </div>
                    </li>
                    <li class="root root-2 ">
                        <div class="root-item no-description">
                            <a href="about_us.php">
                                <div class="title title_font"><span class="title-text">About Us</span><span
                                        class="icon-has-sub fa fa-angle-down"></span></div>
                            </a>
                        </div>

                    </li>
                    
                    <li class="root root-3 ">

                        <div class="root-item no-description">
                            <a href="#">
                                <div class="title title_font"><span class="title-text">Products</span><span
                                        class="icon-has-sub fa fa-angle-down"></span></div>
                            </a>
                        </div>
                        <ul class="menu-items col-md-6 col-xs-12">

                            <li class="menu-item menu-item-121 depth-1 category menucol-1-3  ">


                                <ul class="submenu submenu-depth-2">
                                    <li class="menu-item menu-item-122 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Bestsellers
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-123 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Erectile Dysfunction
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-124 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                ED Sets
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Allergies
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Anti Fungal
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Anti Viral
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Antibiotics
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Anxiety
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Arthritis
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Asthma
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Birth Control
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Blood Pressure
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Cholesterol Lowering
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Depression
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Diabetes
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Gastrointestinal
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Hair Loss
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Heart Disease
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Herbals
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Man's Health
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Muscle Relaxant
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Other
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Pain Relief
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Skincare
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Sleep Aid
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Quit Smoking
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Weight Loss
                                            </a>
                                        </div>
                                    </li>
                                    <li class="menu-item menu-item-125 depth-2 category   ">
                                        <div class="title">
                                            <a href="medicine-category.php">
                                                Woman's Health
                                            </a>
                                        </div>
                                    </li>

                                </ul>
                            </li>



                        </ul>

                    </li>

                    <li class="root root-4 ">
                        <div class="root-item no-description">
                            <a href="Shipping_Policy.php">
                                <div class="title title_font"><span class="title-text">Shipping Policy</span></div>
                            </a>
                        </div>
                    </li>


                    <li class="root root-5 right">
                        <div class="root-item no-description">
                            <a href="testimonial.php">
                                <div class="title title_font"><span class="title-text">Testimonial</span></div>
                            </a>
                        </div>
                    </li>
                    <li class="root root-6 right">
                        <div class="root-item no-description">
                            <a href="contact_us.php">
                                <div class="title title_font"><span class="title-text">Contact Us</span><span
                                        class="icon-has-sub fa fa-angle-down"></span></div>
                            </a>
                        </div>

                    </li>
                    <!-- <li class="root root-5 ">
                                        <div class="root-item no-description right">
                                            <a href="login.php">
                                                <div class="title title_font "><span class="title-text">Sign In
                                                    </span><span class="icon-has-sub fa fa-angle-down"></span></div>
                                            </a>
                                        </div>
                                    </li> -->

                </ul>
            </nav>
        </div>
    </div>
</div>