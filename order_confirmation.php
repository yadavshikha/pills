<!doctype html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
include ('partials/head.php');
?>


<body id="authentication"
    class="lang-en country-us currency-usd layout-full-width page-authentication tax-display-disabled page-customer-account fullwidth">




    <main>

        <?php
include ('partials/header.php');
?>

        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <!--END SLIDER SHOW -->


        <aside id="notifications">
            <div class="container">
            </div>
        </aside>
        <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">
                <div id="content-wrapper">
                    <section id="main">
                        <h2 style="display:none">.</h2>
                        <section id="content-hook_order_confirmation" class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="h1 card-title">
                                            <i class="material-icons done"></i>Your order is confirmed
                                        </h3>
                                        <p>
                                            An email has been sent to your mail address sadswgd@gmail.com.
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="content" class="page-content page-order-confirmation card">
                            <div class="card-block">
                                <div class="row">
                                    <div id="order-items" class="col-md-8">
                                        <h3 class="card-title h3">Order items</h3>

                                        <div class="order-confirmation-table">
                                            <div class="order-line row">
                                                <div class="col-sm-2 col-xs-3">
                                                    <span class="image">
                                                        <img
                                                            src="https://demo.fieldthemes.com/ps_medicine/home1/80-medium_default/aliquam-tincidunt-mauris.jpg">
                                                    </span>
                                                </div>
                                                <div class="col-sm-4 col-xs-9 details">
                                                    <span>Aliquam tincidunt mauris.</span>

                                                </div>
                                                <div class="col-sm-6 col-xs-12 qty">
                                                    <div class="row">
                                                        <div class="col-xs-5 text-sm-right text-xs-left">$32.40</div>
                                                        <div class="col-xs-2">1</div>
                                                        <div class="col-xs-5 text-xs-right bold">$32.40</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <table class="table">
                                            </table>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>Subtotal</td>
                                                        <td>$32.40</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shipping and handling</td>
                                                        <td>$7.00</td>
                                                    </tr>
                                                    <tr class="font-weight-bold">
                                                        <td><span class="text-uppercase">Total</span> (tax excl.)</td>
                                                        <td>$39.40</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="order-details" class="col-md-4">
                                        <h3 class="h3 card-title">Order details:</h3>
                                        <ul>
                                            <li>Order reference: MACKMBVPC</li>
                                            <li>Payment method: Wire payment</li>
                                            <li>
                                                Shipping method: My carrier<br>
                                                <em>Delivery next day!</em>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <section id="content-hook_payment_return" class="card definition-list">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            Your order on medicine_home1 is complete.<br>
                                            Please send us a bank wire with:
                                        </p>
                                        <dl>
                                            <dt>Amount</dt>
                                            <dd>$39.40</dd>
                                            <dt>Name of account owner</dt>
                                            <dd>___________</dd>
                                            <dt>Please include these details</dt>
                                            <dd>___________</dd>
                                            <dt>Bank name</dt>
                                            <dd>___________</dd>
                                        </dl>
                                        <p>
                                            Please specify your order reference MACKMBVPC in the bankwire
                                            description.<br>
                                            We've also sent you this information by e-mail.
                                        </p>
                                        <strong>Your order will be sent as soon as we receive payment.</strong>
                                        <p>
                                            If you have questions, comments or concerns, please contact our <a
                                                href="https://demo.fieldthemes.com/ps_medicine/home1/en/contact-us">expert
                                                customer support team</a>.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="content" class="page-content page-order-confirmation card">
                        <div id="registration-form" class="card">
                            <div class="card-block">
                                <h4 class="h4">Save time on your next order, sign up now</h4>
                                <form
                                    action="https://demo.fieldthemes.com/ps_medicine/home1/en/order-confirmation?id_cart=1175&amp;id_module=26&amp;id_order=51&amp;key=f97aefac0ca42f0e0d0851e2b0adf66d"
                                    id="customer-form" class="js-customer-form" method="post">
                                    <section>
                                        <input type="hidden" name="id_customer" value="">
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label">
                                                Social title
                                            </label>
                                            <div class="col-md-6 form-control-valign">
                                                <label class="radio-inline">
                                                    <span class="custom-radio">
                                                        <input name="id_gender" type="radio" value="1">
                                                        <span></span>
                                                    </span>
                                                    Mr.
                                                </label>
                                                <label class="radio-inline">
                                                    <span class="custom-radio">
                                                        <input name="id_gender" type="radio" value="2">
                                                        <span></span>
                                                    </span>
                                                    Mrs.
                                                </label>
                                            </div>

                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label required">
                                                First name
                                            </label>
                                            <div class="col-md-6">
                                                <input class="form-control" name="firstname" type="text" value=""
                                                    required="">
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label required">
                                                Last name
                                            </label>
                                            <div class="col-md-6">
                                                <input class="form-control" name="lastname" type="text" value=""
                                                    required="">
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label required">
                                                Email
                                            </label>
                                            <div class="col-md-6">
                                                <input class="form-control" name="email" type="email" value=""
                                                    required="">
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label required">
                                                Password
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group js-parent-focus">
                                                    <input class="form-control js-child-focus js-visible-password"
                                                        name="password" type="password" value="" pattern=".{5,}"
                                                        required="">
                                                    <span class="input-group-btn">
                                                        <button class="btn" type="button" data-action="show-password"
                                                            data-text-show="Show" data-text-hide="Hide">
                                                            Show
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label">
                                                Birthdate
                                            </label>
                                            <div class="col-md-6">
                                                <input class="form-control" name="birthday" type="text" value=""
                                                    placeholder="MM/DD/YYYY">
                                                <span class="form-control-comment">
                                                    (E.g.: 05/31/1970)
                                                </span>
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                                Optional
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label">
                                            </label>
                                            <div class="col-md-6">
                                                <span class="custom-checkbox">
                                                    <input name="optin" type="checkbox" value="1">
                                                    <span><i class="material-icons checkbox-checked"></i></span>
                                                    <label>Receive offers from our partners</label>
                                                </span>
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                        <div class="form-group row ">
                                            <label class="col-md-3 form-control-label">
                                            </label>
                                            <div class="col-md-6">
                                                <span class="custom-checkbox">
                                                    <input name="newsletter" type="checkbox" value="1">
                                                    <span><i class="material-icons checkbox-checked"></i></span>
                                                    <label>Sign up for our newsletter<br><em>You may unsubscribe at any
                                                            moment.
                                                            For that purpose, please find our contact info in the legal
                                                            notice.</em></label>
                                                </span>
                                            </div>

                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>
                                    </section>
                                    <footer class="form-footer clearfix">
                                        <input type="hidden" name="submitCreate" value="1">

                                        <button class="btn btn-primary form-control-submit pull-xs-right"
                                            data-link-action="save-customer" type="submit">
                                            Save
                                        </button>

                                    </footer>

                                </form>

                            </div>
                        </div>
                        </section>
                        <section id="content-hook-order-confirmation-footer">
                        </section>
                        <footer class="page-footer">

                            <!-- Footer content -->
                        </footer>
                    </section>


                </div>
            </div>

        </section>
        <?php
include ('partials/footer.php');
?>
    </main>
    <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>
</body>

</html>














































