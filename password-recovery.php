<!doctype html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<?php
include ('partials/head.php');
?>

<body id="password"
    class="lang-en country-us currency-usd layout-full-width page-password tax-display-disabled page-customer-account fullwidth">


    <?php
include ('partials/header.php');
?>
  

    <main>
        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <!--END SLIDER SHOW -->


        <aside id="notifications">
            <div class="container">



            </div>
        </aside>

        <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">







                <div id="content-wrapper">

                    <section id="main">
                        <h2 style="display:none">.</h2>



                        <header class="page-header">
                            <h1>
                                Forgot your password?
                            </h1>
                        </header>




                        <section id="content" class="page-content card card-block">


                            <form action="https://demo.fieldthemes.com/ps_medicine/home1/en/password-recovery"
                                method="post">

                                <header>
                                    <p>Please enter the email address you used to register. You will receive a temporary
                                        link to reset your password.</p>
                                </header>

                                <section class="form-fields">
                                    <div class="form-group row">
                                        <label class="col-md-3 form-control-label required">Email address</label>
                                        <div class="col-md-5">
                                            <input type="email" name="email" id="email" value="" class="form-control"
                                                required>
                                        </div>
                                    </div>
                                </section>

                                <footer class="form-footer text-xs-center">
                                    <button class="form-control-submit btn btn-primary" name="submit" type="submit">
                                        Send reset link
                                    </button>
                                </footer>

                            </form>

                        </section>



                        <footer class="page-footer">

                            <a href="login.php" class="account-link">
                                <i class="material-icons">&#xE5CB;</i>
                                <span>Back to login</span>
                            </a>

                        </footer>


                    </section>


                </div>






            </div>

        </section>

        

    </main>

    


    <?php
include ('partials/footer.php');
?>


</body>




</html>