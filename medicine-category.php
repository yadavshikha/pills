<!doctype html>
<html lang="en">



<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<?php
include ('partials/head.php');
?>

<body id="index" class="lang-en country-us currency-usd layout-full-width page-index tax-display-disabled fullwidth">




    <main>
        <?php
include ('partials/header.php');
?>

        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <div id="field_slideshow">
            <div class="field-main-slider  block" style="overflow: hidden;">
                <div id="insideslider_mod" class="outer-slide" style="width: 1920px; height: 700px">
                    <div class="loading">
                        <div class="bg-loading"></div>
                        <div class="icon-loading"></div>
                    </div>
                    <div data-u="slides" style="width: 1920px; height: 700px">

                        <div class="field-main-slider_1">
                            <a href="#">
                                <img class="img-slider" src="modules/fieldslideshow/images/slider-111.jpg" alt=""
                                    data-u="image">
                            </a>
                            <div class="box-slider">
                                <div class="large-slide-title title_font" data-u="caption" data-t="T-*IB"
                                    data-t2="ZML|TR" data-d="-300">
                                    LAST CHANCE TO BUY

                                </div>
                                <div class="big-slide-title title_font" data-u="caption" data-t="ZM*WVR|LB"
                                    data-t2="WVC|R" data-d="-300">
                                    NUTRITIONAL PILLS
                                </div>
                                <div class="small-slide-title title_font" data-u="caption" data-t="TORTUOUS|HL"
                                    data-t2="JDN|B" data-d="-300">
                                    <p>MADE BY NATURE</p>
                                </div>
                                <div class="div-slide-button shop_now" data-u="caption" data-t="B-R*">
                                    <a class="slide-button title_font" href="#">
                                        Shop now
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="field-main-slider_2">
                            <a href="#">
                                <img class="img-slider" src="modules/fieldslideshow/images/slider-222.jpg" alt=""
                                    data-u="image">
                            </a>
                            <div class="box-slider">
                                <div class="large-slide-title title_font" data-u="caption" data-t="ZM*JUP1|T"
                                    data-t2="FLTTRWN|LT">
                                    LAST CHANCE TO BUY

                                </div>
                                <div class="big-slide-title title_font" data-u="caption" data-t="ZM*JUP1|T"
                                    data-t2="FLTTRWN|LT">
                                    OUR ONLINE PHARMACY
                                </div>
                                <div class="small-slide-title title_font" data-u="caption" data-t="ZM*JUP1|L"
                                    data-t2="TORTUOUS|HL">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                </div>
                                <div class="div-slide-button shop_now" data-u="caption" data-t="ZM*JUP1|B">
                                    <a class="slide-button title_font" href="#">
                                        Shop now
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="field-main-slider_3">
                            <a href="#">
                                <img class="img-slider" src="modules/fieldslideshow/images/slider-333.jpg" alt=""
                                    data-u="image">
                            </a>
                            <div class="box-slider">
                                <div class="large-slide-title title_font" data-u="caption" data-t="ZM*WVR|RT"
                                    data-t2="WVC|B" data-d="-300">
                                    Explore your option

                                </div>
                                <div class="big-slide-title title_font" data-u="caption" data-t="ZM*WVR|LB"
                                    data-t2="WVC|T" data-d="-300">
                                    Joint & Ligament
                                </div>
                                <div class="small-slide-title title_font" data-u="caption" data-t="DDGDANCE|RB"
                                    data-t2="WVC|T" data-d="-300">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing</p>
                                </div>
                                <div class="div-slide-button shop_now" data-u="caption" data-t="ZM*WVR|LB"
                                    data-t2="WVC|T" data-d="-300">
                                    <a class="slide-button title_font" href="#">
                                        Shop now
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div data-u="navigator">
                        <div data-u="prototype"></div>
                    </div>
                    <span data-u="arrowleft"><i class="fa fa-angle-left"></i></span>
                    <span data-u="arrowright"><i class="fa fa-angle-right"></i></span>
                </div>

                <script>
                    jQuery(document).ready(function ($) {
                        var _SlideshowTransitions = [{
                            $Duration: 700,
                            $Opacity: 2,
                            $Brother: {
                                $Duration: 1000,
                                $Opacity: 2
                            }
                        }, {
                            $Duration: 1200,
                            $Delay: 20,
                            $Cols: 8,
                            $Rows: 4,
                            $Clip: 15,
                            $During: {
                                $Left: [0.3, 0.7],
                                $Top: [0.3, 0.7]
                            },
                            $SlideOut: true,
                            $FlyDirection: 9,
                            $Formation: $JssorSlideshowFormations$.$FormationStraightStairs,
                            $Assembly: 260,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Clip: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.1,
                            $Outside: true,
                            $Round: {
                                $Left: 1.3,
                                $Top: 2.5
                            }
                        }, {
                            $Duration: 1000,
                            $Zoom: 1,
                            $Rotate: true,
                            $SlideOut: true,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        }, {
                            $Duration: 1000,
                            $Delay: 30,
                            $Cols: 8,
                            $Rows: 4,
                            $Clip: 15,
                            $Formation: $JssorSlideshowFormations$.$FormationStraightStairs,
                            $Assembly: 2050,
                            $Easing: $JssorEasing$.$EaseInQuad
                        }];
                        var _CaptionTransitions = [];
                        _CaptionTransitions["L"] = {
                            $Duration: 900,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R"] = {
                            $Duration: 900,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T"] = {
                            $Duration: 900,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B"] = {
                            $Duration: 900,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL"] = {
                            $Duration: 900,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR"] = {
                            $Duration: 900,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL"] = {
                            $Duration: 900,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR"] = {
                            $Duration: 900,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutBack,
                                $Top: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutBack,
                                $Top: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutBack,
                                $Top: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR|IB"] = {
                            $Duration: 1200,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutBack,
                                $Top: $JssorEasing$.$EaseInOutBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR|IE"] = {
                            $Duration: 1200,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutExpo,
                                $Top: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutExpo,
                                $Top: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutExpo,
                                $Top: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR|EP"] = {
                            $Duration: 1200,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutExpo,
                                $Top: $JssorEasing$.$EaseInOutExpo
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L*"] = {
                            $Duration: 900,
                            $Rotate: -0.05,
                            $FlyDirection: 1,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R*"] = {
                            $Duration: 900,
                            $Rotate: 0.05,
                            $FlyDirection: 2,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T*"] = {
                            $Duration: 900,
                            $Rotate: -0.05,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B*"] = {
                            $Duration: 900,
                            $Rotate: 0.05,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL*"] = {
                            $Duration: 900,
                            $Rotate: -0.05,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR*"] = {
                            $Duration: 900,
                            $Rotate: 0.05,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL*"] = {
                            $Duration: 900,
                            $Rotate: -0.05,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR*"] = {
                            $Duration: 900,
                            $Rotate: 0.05,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInOutSine
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR*IE"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.3,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR*IB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.3,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L-*IB"] = {
                            $Duration: 900,
                            $Rotate: -0.5,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["R-*IB"] = {
                            $Duration: 900,
                            $Rotate: 0.5,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["T-*IB"] = {
                            $Duration: 900,
                            $Rotate: -0.5,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleVertical: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Top: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["B-*IB"] = {
                            $Duration: 900,
                            $Rotate: 0.5,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleVertical: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Top: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["TL-*IB"] = {
                            $Duration: 900,
                            $Rotate: -0.5,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.7,
                            $ScaleVertical: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["TR-*IB"] = {
                            $Duration: 900,
                            $Rotate: 0.5,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.7,
                            $ScaleVertical: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["BL-*IB"] = {
                            $Duration: 900,
                            $Rotate: -0.5,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.7,
                            $ScaleVertical: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["BR-*IB"] = {
                            $Duration: 900,
                            $Rotate: 0.5,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInBack
                            },
                            $ScaleHorizontal: 0.7,
                            $ScaleVertical: 0.7,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.2, 0.8]
                            }
                        };
                        _CaptionTransitions["L*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 4,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 8,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TL*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["TR*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BL*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["BR*IW"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Rotate: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["R|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["T|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["B|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["TL|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["TR|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["BL|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["BR|IE*IE"] = {
                            $Duration: 1800,
                            $Zoom: 11,
                            $Rotate: -1.5,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutElastic,
                                $Top: $JssorEasing$.$EaseInOutElastic,
                                $Zoom: $JssorEasing$.$EaseInElastic,
                                $Rotate: $JssorEasing$.$EaseInOutElastic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Zoom: [0, 0.8],
                                $Opacity: [0, 0.7]
                            },
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["CLIP"] = {
                            $Duration: 900,
                            $Clip: 15,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["CLIP|LR"] = {
                            $Duration: 900,
                            $Clip: 3,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["CLIP|TB"] = {
                            $Duration: 900,
                            $Clip: 12,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["CLIP|L"] = {
                            $Duration: 900,
                            $Clip: 1,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["CLIP|R"] = {
                            $Duration: 900,
                            $Clip: 2,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["CLIP|T"] = {
                            $Duration: 900,
                            $Clip: 4,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["CLIP|B"] = {
                            $Duration: 900,
                            $Clip: 8,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["MCLIP|L"] = {
                            $Duration: 900,
                            $Clip: 1,
                            $Move: true,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            }
                        };
                        _CaptionTransitions["MCLIP|R"] = {
                            $Duration: 900,
                            $Clip: 2,
                            $Move: true,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            }
                        };
                        _CaptionTransitions["MCLIP|T"] = {
                            $Duration: 900,
                            $Clip: 4,
                            $Move: true,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            }
                        };
                        _CaptionTransitions["MCLIP|B"] = {
                            $Duration: 900,
                            $Clip: 8,
                            $Move: true,
                            $Easing: {
                                $Clip: $JssorEasing$.$EaseInOutCubic
                            }
                        };
                        _CaptionTransitions["ZM"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM|P30"] = {
                            $Duration: 900,
                            $Zoom: 1.3,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM|P50"] = {
                            $Duration: 900,
                            $Zoom: 1.5,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM|P70"] = {
                            $Duration: 900,
                            $Zoom: 1.7,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM|P80"] = {
                            $Duration: 900,
                            $Zoom: 1.8,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMF|2"] = {
                            $Duration: 900,
                            $Zoom: 3,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMF|3"] = {
                            $Duration: 900,
                            $Zoom: 4,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMF|4"] = {
                            $Duration: 900,
                            $Zoom: 5,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMF|5"] = {
                            $Duration: 900,
                            $Zoom: 6,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMF|10"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|L"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|R"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|T"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|B"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|TL"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|TR"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|BL"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|BR"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|L"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|R"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|T"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|B"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|TL"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|TR"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|BL"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZML|IE|BR"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInElastic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|L"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|R"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|T"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|B"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|TL"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|TR"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|BL"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZMS|BR"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*JDN|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JUP|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["ZM*JDN|LB*"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.75
                            }
                        };
                        _CaptionTransitions["ZM*JDN|RB*"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.75
                            }
                        };
                        _CaptionTransitions["ZM*JDN1|L"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JDN1|R"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JDN1|T"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JDN1|B"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JUP1|L"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JUP1|R"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JUP1|T"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*JUP1|B"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Zoom: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WVC|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVC|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WVR|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3
                            }
                        };
                        _CaptionTransitions["ZM*WV*J1|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3
                            }
                        };
                        _CaptionTransitions["ZM*WV*J2|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J2|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ZM*WV*J3|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5
                            }
                        };
                        _CaptionTransitions["ZM*WV*J3|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5
                            }
                        };
                        _CaptionTransitions["RTT"] = {
                            $Duration: 900,
                            $Rotate: 1,
                            $Easing: {
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInQuad
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT|90"] = {
                            $Duration: 900,
                            $Rotate: 1,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["RTT|360"] = {
                            $Duration: 900,
                            $Rotate: 1,
                            $Easing: {
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInQuad
                            },
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT|0"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInQuad
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT|2"] = {
                            $Duration: 900,
                            $Zoom: 3,
                            $Rotate: 1,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInQuad
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT|3"] = {
                            $Duration: 900,
                            $Zoom: 4,
                            $Rotate: 1,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInQuad
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT|4"] = {
                            $Duration: 900,
                            $Zoom: 5,
                            $Rotate: 1,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInQuad
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT|5"] = {
                            $Duration: 900,
                            $Zoom: 6,
                            $Rotate: 1,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInExpo
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTT|10"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $Easing: {
                                $Zoom: $JssorEasing$.$EaseInExpo,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInExpo
                            },
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|L"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|R"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|T"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|B"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|TL"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|TR"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|BL"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTL|BR"] = {
                            $Duration: 900,
                            $Zoom: 11,
                            $Rotate: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.8
                            }
                        };
                        _CaptionTransitions["RTTS|L"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|R"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|T"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|B"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|TL"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuad,
                                $Top: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|TR"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuad,
                                $Top: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|BL"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuad,
                                $Top: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTTS|BR"] = {
                            $Duration: 900,
                            $Zoom: 1,
                            $Rotate: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuad,
                                $Top: $JssorEasing$.$EaseInQuad,
                                $Zoom: $JssorEasing$.$EaseInQuad,
                                $Rotate: $JssorEasing$.$EaseInQuad,
                                $Opacity: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 1.2
                            }
                        };
                        _CaptionTransitions["RTT*JDN|L"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|R"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|T"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|B"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|L"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|R"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|T"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|B"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JUP|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: 0.2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["RTT*JDN|LB*"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.75,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN|RB*"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.75,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|L"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|R"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|T"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|B"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|L"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|R"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|T"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|B"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|TL"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|TR"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JDN1|BL"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|TL"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|TR"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*JUP1|BL"] = {
                            $Duration: 1200,
                            $Zoom: 6,
                            $Rotate: 0.25,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic,
                                $Opacity: $JssorEasing$.$EaseLinear,
                                $Rotate: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WVC|LT"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|LB"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|RT"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|RB"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|TL"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|TR"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|BL"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVC|BR"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|LT"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|LB"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|RT"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|RB"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 2,
                            $ScaleVertical: 0.3,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|TL"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|TR"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|BL"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WVR|BR"] = {
                            $Duration: 1500,
                            $Zoom: 11,
                            $Rotate: 0.3,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 2,
                            $Opacity: 2
                        };
                        _CaptionTransitions["RTT*WV*J1|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.3,
                                $Top: 0.5,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J1|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.8,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.5]
                            },
                            $Round: {
                                $Left: 0.5,
                                $Top: 0.3,
                                $Rotate: 0.4
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J2|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -0.6,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.4,
                            $ScaleVertical: 0.8,
                            $Opacity: 2,
                            $Round: {
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|LT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|LB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|RT"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|RB"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInSine,
                                $Top: $JssorEasing$.$EaseOutJump,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Top: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|TL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|TR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|BL"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["RTT*WV*J3|BR"] = {
                            $Duration: 1200,
                            $Zoom: 11,
                            $Rotate: -1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseInSine,
                                $Zoom: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 0.5,
                                $Rotate: 0.5
                            }
                        };
                        _CaptionTransitions["DDG|TL"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 0.8
                            }
                        };
                        _CaptionTransitions["DDG|TR"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 0.8
                            }
                        };
                        _CaptionTransitions["DDG|BL"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 0.8
                            }
                        };
                        _CaptionTransitions["DDG|BR"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 0.8
                            }
                        };
                        _CaptionTransitions["DDGDANCE|LT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGDANCE|RT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGDANCE|LB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGDANCE|RB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseInJump,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.8],
                                $Top: [0, 0.8]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGPET|LT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.05,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGPET|LB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.05,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGPET|RT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.05,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["DDGPET|RB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.05,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.8,
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["FLTTR|L"] = {
                            $Duration: 900,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.1,
                            $Opacity: 2,
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTR|R"] = {
                            $Duration: 900,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.1,
                            $Opacity: 2,
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTR|T"] = {
                            $Duration: 900,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.1,
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTR|B"] = {
                            $Duration: 900,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.1,
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|LT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|LB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|RT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|RB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|TL"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine,
                                $Left: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7],
                                $Left: [0.1, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|TR"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine,
                                $Left: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7],
                                $Left: [0.1, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|BL"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine,
                                $Left: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7],
                                $Left: [0.1, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["FLTTRWN|BR"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseInOutSine,
                                $Left: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7],
                                $Left: [0.1, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["LATENCY|LT"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|LB"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|RT"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|RB"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInOutSine,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.1, 0.7]
                            },
                            $Round: {
                                $Top: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|TL"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseOutSine,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.1, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|TR"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseOutSine,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.1, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|BL"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseOutSine,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.1, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.4
                            }
                        };
                        _CaptionTransitions["LATENCY|BR"] = {
                            $Duration: 1200,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseOutSine,
                                $Zoom: $JssorEasing$.$EaseInOutQuad
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.1, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 0.4
                            }
                        };
                        _CaptionTransitions["TORTUOUS|HL"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 1,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|HR"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 2,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|VB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 8,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|VT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 4,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|LT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|LB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|RT"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|RB"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Top: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleVertical: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|TL"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|TR"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|BL"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["TORTUOUS|BR"] = {
                            $Duration: 1800,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Zoom: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.2,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["SPACESHIP|LT"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuint,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Opacity: $JssorEasing$.$EaseInQuint
                            },
                            $ScaleHorizontal: 1,
                            $ScaleVertical: 0.1,
                            $Opacity: 2
                        };
                        _CaptionTransitions["SPACESHIP|LB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: -0.1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuint,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Opacity: $JssorEasing$.$EaseInQuint
                            },
                            $ScaleHorizontal: 1,
                            $ScaleVertical: 0.1,
                            $Opacity: 2
                        };
                        _CaptionTransitions["SPACESHIP|RT"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuint,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Opacity: $JssorEasing$.$EaseInQuint
                            },
                            $ScaleHorizontal: 1,
                            $ScaleVertical: 0.1,
                            $Opacity: 2
                        };
                        _CaptionTransitions["SPACESHIP|RB"] = {
                            $Duration: 1200,
                            $Zoom: 3,
                            $Rotate: 0.1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInQuint,
                                $Top: $JssorEasing$.$EaseInWave,
                                $Opacity: $JssorEasing$.$EaseInQuint
                            },
                            $ScaleHorizontal: 1,
                            $ScaleVertical: 0.1,
                            $Opacity: 2
                        };
                        _CaptionTransitions["ATTACK|LT"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInExpo,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.1,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.3, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|LB"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInExpo,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.1,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.3, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|RT"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInExpo,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.1,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.3, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|RB"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInExpo,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.1,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.7],
                                $Top: [0.3, 0.7]
                            },
                            $Round: {
                                $Top: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|TL"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInExpo
                            },
                            $ScaleHorizontal: 0.1,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.3, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|TR"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInExpo
                            },
                            $ScaleHorizontal: 0.1,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.3, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|BL"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInExpo
                            },
                            $ScaleHorizontal: 0.1,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.3, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["ATTACK|BR"] = {
                            $Duration: 1500,
                            $Zoom: 1,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseInExpo
                            },
                            $ScaleHorizontal: 0.1,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.3, 0.7],
                                $Top: [0, 0.7]
                            },
                            $Round: {
                                $Left: 1.3
                            }
                        };
                        _CaptionTransitions["LISTV|L"] = {
                            $Duration: 1500,
                            $Clip: 4,
                            $FlyDirection: 1,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTV|R"] = {
                            $Duration: 1500,
                            $Clip: 4,
                            $FlyDirection: 2,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTH|L"] = {
                            $Duration: 1500,
                            $Clip: 1,
                            $FlyDirection: 1,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTH|R"] = {
                            $Duration: 1500,
                            $Clip: 1,
                            $FlyDirection: 2,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTVC|L"] = {
                            $Duration: 1500,
                            $Clip: 12,
                            $FlyDirection: 1,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTVC|R"] = {
                            $Duration: 1500,
                            $Clip: 12,
                            $FlyDirection: 2,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTVC|B"] = {
                            $Duration: 1500,
                            $Clip: 12,
                            $FlyDirection: 8,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleVertical: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Top: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTVC|T"] = {
                            $Duration: 1500,
                            $Clip: 12,
                            $FlyDirection: 4,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleVertical: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Top: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTHC|L"] = {
                            $Duration: 1500,
                            $Clip: 3,
                            $FlyDirection: 1,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTHC|R"] = {
                            $Duration: 1500,
                            $Clip: 3,
                            $FlyDirection: 2,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleHorizontal: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTHC|B"] = {
                            $Duration: 1500,
                            $Clip: 3,
                            $FlyDirection: 8,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleVertical: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Top: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["LISTHC|T"] = {
                            $Duration: 1500,
                            $Clip: 3,
                            $FlyDirection: 4,
                            $Easing: $JssorEasing$.$EaseInOutCubic,
                            $ScaleVertical: 0.8,
                            $ScaleClip: 0.8,
                            $Opacity: 2,
                            $During: {
                                $Top: [0.4, 0.6],
                                $Clip: [0, 0.4],
                                $Opacity: [0.4, 0.6]
                            }
                        };
                        _CaptionTransitions["WV|L"] = {
                            $Duration: 1800,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["WV|R"] = {
                            $Duration: 1800,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["WV|T"] = {
                            $Duration: 1200,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["WV|B"] = {
                            $Duration: 1200,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["WVC|L"] = {
                            $Duration: 1800,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["WVC|R"] = {
                            $Duration: 1800,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["WVC|T"] = {
                            $Duration: 1200,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["WVC|B"] = {
                            $Duration: 1200,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["WVR|L"] = {
                            $Duration: 1800,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["WVR|R"] = {
                            $Duration: 1800,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.3,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["JDN|L"] = {
                            $Duration: 2000,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutJump
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["JDN|R"] = {
                            $Duration: 2000,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutJump
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["JDN|T"] = {
                            $Duration: 1500,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["JDN|B"] = {
                            $Duration: 1500,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutJump,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["JUP|L"] = {
                            $Duration: 2000,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInJump
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["JUP|R"] = {
                            $Duration: 2000,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInJump
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.4,
                            $Opacity: 2,
                            $Round: {
                                $Top: 2.5
                            }
                        };
                        _CaptionTransitions["JUP|T"] = {
                            $Duration: 1500,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["JUP|B"] = {
                            $Duration: 1500,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInJump,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.3,
                            $ScaleVertical: 0.6,
                            $Opacity: 2,
                            $Round: {
                                $Left: 1.5
                            }
                        };
                        _CaptionTransitions["FADE"] = {
                            $Duration: 900,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JDN|L"] = {
                            $Duration: 1200,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JDN|R"] = {
                            $Duration: 1200,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JDN|T"] = {
                            $Duration: 1200,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JDN|B"] = {
                            $Duration: 1200,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JUP|L"] = {
                            $Duration: 900,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JUP|R"] = {
                            $Duration: 900,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JUP|T"] = {
                            $Duration: 900,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["FADE*JUP|B"] = {
                            $Duration: 900,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.6,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["L-JDN"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["R-JDN"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["T-JDN"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["B-JDN"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["L-JUP"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["R-JUP"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInCubic
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.5,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["T-JUP"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["B-JUP"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInCubic,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["L-WVC"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.3,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["R-WVC"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseOutWave
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.3,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["T-WVC"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 5,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["B-WVC"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseOutWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["L-WVR"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 9,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.3,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["R-WVR"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseLinear,
                                $Top: $JssorEasing$.$EaseInWave
                            },
                            $ScaleHorizontal: 0.8,
                            $ScaleVertical: 0.3,
                            $During: {
                                $Top: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["T-WVR"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 6,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["B-WVR"] = {
                            $Duration: 1200,
                            $Opacity: 2,
                            $FlyDirection: 10,
                            $Easing: {
                                $Left: $JssorEasing$.$EaseInWave,
                                $Top: $JssorEasing$.$EaseLinear
                            },
                            $ScaleHorizontal: 0.2,
                            $ScaleVertical: 0.8,
                            $During: {
                                $Left: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP-FADE"] = {
                            $Duration: 1200,
                            $Clip: 15,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP|LR-FADE"] = {
                            $Duration: 1200,
                            $Clip: 3,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP|TB-FADE"] = {
                            $Duration: 1200,
                            $Clip: 12,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP|L-FADE"] = {
                            $Duration: 1200,
                            $Clip: 1,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP|R-FADE"] = {
                            $Duration: 1200,
                            $Clip: 2,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP|T-FADE"] = {
                            $Duration: 1200,
                            $Clip: 4,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["CLIP|B-FADE"] = {
                            $Duration: 1200,
                            $Clip: 8,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["MCLIP|L-FADE"] = {
                            $Duration: 1200,
                            $Clip: 1,
                            $Move: true,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["MCLIP|R-FADE"] = {
                            $Duration: 1200,
                            $Clip: 2,
                            $Move: true,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["MCLIP|T-FADE"] = {
                            $Duration: 1200,
                            $Clip: 4,
                            $Move: true,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["MCLIP|B-FADE"] = {
                            $Duration: 1200,
                            $Clip: 8,
                            $Move: true,
                            $Opacity: 1.7,
                            $During: {
                                $Clip: [0.5, 0.5],
                                $Opacity: [0, 0.5]
                            }
                        };
                        _CaptionTransitions["L*CLIP"] = {
                            $Duration: 1200,
                            $Clip: 12,
                            $FlyDirection: 1,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["R*CLIP"] = {
                            $Duration: 1200,
                            $Clip: 12,
                            $FlyDirection: 2,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $ScaleHorizontal: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T*CLIP"] = {
                            $Duration: 1200,
                            $Clip: 3,
                            $FlyDirection: 4,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["B*CLIP"] = {
                            $Duration: 1200,
                            $Clip: 3,
                            $FlyDirection: 8,
                            $Easing: $JssorEasing$.$EaseInCubic,
                            $ScaleVertical: 0.6,
                            $Opacity: 2
                        };
                        _CaptionTransitions["T-L*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 5,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.33],
                                $Top: [0.67, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["T-R*"] = {
                            $Duration: 1500,
                            $Rotate: 1,
                            $FlyDirection: 6,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.33],
                                $Top: [0.67, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["B-L*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 9,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.33],
                                $Top: [0.67, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["B-R*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 10,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.33],
                                $Top: [0.67, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["L-T*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 5,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.67, 0.33],
                                $Top: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["L-B*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 10,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.67, 0.33],
                                $Top: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["R-T*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 6,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.67, 0.33],
                                $Top: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["R-B*"] = {
                            $Duration: 1500,
                            $Rotate: -1,
                            $FlyDirection: 10,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0.67, 0.33],
                                $Top: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["FADE-L*"] = {
                            $Duration: 1500,
                            $Rotate: 6.25,
                            $FlyDirection: 1,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["FADE-R*"] = {
                            $Duration: 1500,
                            $Rotate: 6.25,
                            $FlyDirection: 2,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleHorizontal: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Left: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["FADE-T*"] = {
                            $Duration: 1500,
                            $Rotate: 6.25,
                            $FlyDirection: 4,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        _CaptionTransitions["FADE-B*"] = {
                            $Duration: 1500,
                            $Rotate: 6.25,
                            $FlyDirection: 8,
                            $Easing: $JssorEasing$.$EaseLinear,
                            $ScaleVertical: 0.5,
                            $Opacity: 2,
                            $During: {
                                $Top: [0, 0.33],
                                $Rotate: [0, 0.33]
                            },
                            $Round: {
                                $Rotate: 0.25
                            }
                        };
                        var options = {
                            $FillMode: 2,
                            $AutoPlay: true,
                            $AutoPlayInterval: 3000,
                            $PauseOnHover: 1,
                            $ArrowKeyNavigation: true,
                            $SlideEasing: $JssorEasing$.$EaseOutQuint,
                            $SlideDuration: 800,
                            $MinDragOffsetToSlide: 20,


                            $SlideSpacing: 0,
                            $DisplayPieces: 1,
                            $ParkingPosition: 0,
                            $UISearchMode: 1,
                            $PlayOrientation: 1,
                            $DragOrientation: 1,

                            $SlideshowOptions: {
                                $Class: $JssorSlideshowRunner$,
                                $Transitions: _SlideshowTransitions,
                                $TransitionsOrder: 0,
                                $ShowLink: true
                            },

                            $CaptionSliderOptions: {
                                $Class: $JssorCaptionSlider$,
                                $CaptionTransitions: _CaptionTransitions,
                                $PlayInMode: 10,
                                $PlayOutMode: 4
                            },

                            $BulletNavigatorOptions: {
                                $Class: $JssorBulletNavigator$,
                                $ChanceToShow: 2,
                                $AutoCenter: 1,
                                $Steps: 1,
                                $Lanes: 1,
                                $SpacingX: 8,
                                $SpacingY: 8,
                                $Orientation: 1
                            },

                            $ArrowNavigatorOptions: {
                                $Class: $JssorArrowNavigator$,
                                $ChanceToShow: 2,
                                $AutoCenter: 2,
                                $Steps: 1
                            }
                        };

                        var insideslider_mod = new $JssorSlider$("insideslider_mod", options);

                        $('.homepage-slideshow [data-u="arrowleft"]').on('click', function () {
                            insideslider_mod.$Prev();
                        });
                        $('.homepage-slideshow [data-u="arrowleft"]').on('click', function () {
                            insideslider_mod.$Next();
                        });

                        function ScaleSlider() {
                            var cfgWidth = 1920;
                            var cfgHeight = 700;

                            var parentWidth = insideslider_mod.$Elmt.parentNode.clientWidth;
                            var slideCurrWidth = $('#insideslider_mod').outerWidth();

                            var baseWidthMax = 1200;
                            var slideWrapRate = baseWidthMax / cfgHeight;

                            var arrowleft = $('#insideslider_mod [data-u="arrowleft"]');
                            var arrowright = $('#insideslider_mod [data-u="arrowright"]');
                            if (cfgWidth <= baseWidthMax) {
                                arrowleft.css({
                                    'left': 30
                                });
                                arrowright.css({
                                    'right': 30
                                });

                            } else {
                                arrowleft.css({
                                    'left': ((cfgWidth - baseWidthMax) / 2) + 30
                                });
                                arrowright.css({
                                    'right': ((cfgWidth - baseWidthMax) / 2) + 30
                                });
                            }

                            $('#insideslider_mod').css({
                                'left': '50%',
                                'margin-left': -(slideCurrWidth / 2)
                            })
                            if (parentWidth) {


                                if (cfgWidth > baseWidthMax) {
                                    if (parentWidth <= baseWidthMax) {
                                        insideslider_mod.$ScaleHeight(parentWidth / slideWrapRate);
                                    } else {
                                        insideslider_mod.$ScaleHeight(cfgHeight);
                                    }
                                } else {
                                    insideslider_mod.$ScaleWidth(Math.min(cfgWidth, parentWidth));
                                }
                            } else {
                                window.setTimeout(ScaleSlider, 30);
                            }
                        }
                        ScaleSlider();
                        if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                            $(window).on('resize', ScaleSlider);
                        }

                        $(window).bind("load", ScaleSlider);
                        $(window).bind("resize", ScaleSlider);
                        $(window).bind("orientationchange", ScaleSlider);
                    });
                    jQuery(window).on('load', function () {
                        jQuery('#insideslider_mod .loading').fadeOut();
                    });
                </script>
            </div>
        </div>
        <!--END SLIDER SHOW -->
        <aside id="notifications">
            <div class="container">

            </div>
        </aside>

        <section id="wrapper"style="margin-bottom: 2rem;">
            <h2 style="display:none">.</h2>
            <div class="container">

                <div id="content-wrapper">

                    <section id="main">
                        <h2 style="display:none">.</h2>

                        <section id="content" class="page-home">

                            <div class="row">
                                <div class="col-xs-12 col-md-2">
                                    <div id="fieldblockcategories" class="block horizontal_mode clearfix">
                                        <div class="text2-border">
                                            <h2 class="title_font">
                                                <a class="title_text">
                                                    Product Categories
                                                </a>
                                            </h2>
                                        </div>
                                        <div class="box_categories">
                                            <div class="row">
                                                <div id="field_content">
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Bestsellers">
                                                            Bestsellers
                                                        </a>
                                                    </div>
                                                   
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Erectile Dysfunction">
                                                            Erectile Dysfunction
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="ED Sets">
                                                            ED Sets
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Allergies">
                                                            Allergies
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Anti Fungal">
                                                            Anti Fungal
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Anti Viral">
                                                            Anti Viral
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Antibiotics ">
                                                            Antibiotics
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Anxiety ">
                                                            Anxiety
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Arthritis  ">
                                                            Arthritis
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Asthma">
                                                            Asthma
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Birth Control">
                                                            Birth Control
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Blood Pressure">
                                                            Blood Pressure
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Cholesterol Lowering">
                                                            Cholesterol Lowering
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Depression">
                                                            Depression
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Diabetes">
                                                            Diabetes
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Gastrointestinal">
                                                            Gastrointestinal
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Hair Loss">
                                                            Hair Loss
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Heart Disease">
                                                            Heart Disease
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Herbals">
                                                            Herbals
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Man's Health ">
                                                            Man's Health
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Muscle Relaxant">
                                                            Muscle Relaxant
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Other">
                                                            Other
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title=" Pain Relief">
                                                            Pain Relief
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Skincare">
                                                            Skincare
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Sleep Aid">
                                                            Sleep Aid
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Quit Smoking">
                                                            Quit Smoking
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Weight Loss">
                                                            Weight Loss
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Woman's Health ">
                                                            Woman's Health
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                </div>
                                <div class="col-xs-12 col-md-10">
                                    <div id="fieldtabproductsisotope" class="horizontal_mode">
                                        <ul class="fieldtabproductsisotope-filters">
                                            <li class="fieldtabproductsisotope-filter title_font">
                                                <a href="#" data-filter="new-arrivals" class="active">
                                                    <span class="text">Bestsellers</span> </a>
                                            </li>
                                        </ul>
                                        <div class="row">
                                            <div class="fieldtabproductsisotope-products">
                                                <ul class="isotope-grid" style="position: relative; height: 640px;">
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals sale-products"
                                                        style="position: absolute; left: 0px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="14" data-id-product-attribute="13"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class=" thumbnail product-thumbnail ">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="https://demo.fieldthemes.com/ps_medicine/home1/79-large_default/integer-cursus-auctor-ex.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                        <div class="conditions-box">
                                                                        </div>
                                                                        <span class="item-countdown">
                                                                            <span class="bg_tranp"></span>
                                                                            <span class="item-countdown-time"
                                                                                data-time="2023-03-25 00:00:00"><span
                                                                                    class="section_cout"><span
                                                                                        class="Days">394
                                                                                    </span><span
                                                                                        class="text">Days</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Hours">18
                                                                                    </span><span
                                                                                        class="text">Hours</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Minutes">49
                                                                                    </span><span
                                                                                        class="text">Mins</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Seconds">55
                                                                                    </span><span
                                                                                        class="text">Secs</span></span></span>
                                                                        </span>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 225px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="21" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg" alt=""
                                                                                    style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="21">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 450px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="17" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../81-large_default/quisque-at-orci-gravid-.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <!-- <div class="conditions-box">
                                                                            <span class="sale_product">Sale</span>

                                                                        </div> -->
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="17">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 675px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="18" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../82-large_default/aliquam-tincidunt-mauris.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>

                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="right-product">
                                                                                </div> -->

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>
                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="18">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals sale-products"
                                                        style="position: absolute; left: 0px; top: 320px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="16" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../80-large_default/aliquam-tincidunt-mauris.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <span class="item-countdown">
                                                                            <span class="bg_tranp"></span>
                                                                            <span class="item-countdown-time"
                                                                                data-time="2023-03-22 00:00:00"><span
                                                                                    class="section_cout"><span
                                                                                        class="Days">391
                                                                                    </span><span
                                                                                        class="text">Days</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Hours">18
                                                                                    </span><span
                                                                                        class="text">Hours</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Minutes">49
                                                                                    </span><span
                                                                                        class="text">Mins</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Seconds">55
                                                                                    </span><span
                                                                                        class="text">Secs</span></span></span>
                                                                        </span>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="16">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 225px; top: 320px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="20" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../84-large_default/fusce-porttitor-augue-lectus.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <!-- <div class="conditions-box">
                                                                            <span class="sale_product">Sale</span>

                                                                        </div> -->
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="20">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals hot-products"
                                                        style="position: absolute; left: 450px; top: 320px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="12" data-id-product-attribute="1"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../78-large_default/nam-mollis-porta-facilisis.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <!-- <div class="conditions-box">
                                                                            <span class="new_product">New</span>

                                                                        </div> -->
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="12">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 675px; top: 320px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="19" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../83-large_default/auctor-ex-id-accumsan.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <!-- <div class="conditions-box">
                                                                            <span class="new_product">New</span>

                                                                        </div> -->
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>
                                                                    <!-- <div class="right-product">
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="product-categories/19-auctor-ex-id-accumsan.html">Auctor
                                                                                        ex id accumsan</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$40.37</span>






                                                                                </div>
                                                                            </div>
                                                                        </div> -->
                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="19">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item hot-products"
                                                        style="position: absolute; left: 0px; top: 640px; display: none;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="36" data-id-product-attribute="19"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../106-large_default/aliquam-tincidunt-mauris.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="36">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item hot-products"
                                                        style="position: absolute; left: 225px; top: 640px; display: none;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="29" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../118-large_default/mollis-porta-facilisis.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="29">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit" disabled="">
                                                                                <i class="fa fa-ban"></i>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item hot-products"
                                                        style="position: absolute; left: 450px; top: 640px; display: none;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="37" data-id-product-attribute="22"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../109-large_default/aliquam-tincidunt-mauris.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="37">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item hot-products"
                                                        style="position: absolute; left: 675px; top: 640px; display: none;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="33" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../111-large_default/porta-facilisis-.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>
                                                                    <!-- <div class="right-product">
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="most-view/33-porta-facilisis-.html">Porta
                                                                                        facilisis.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$35.40</span>






                                                                                </div>
                                                                            </div>
                                                                        </div> -->
                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="33">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit" disabled="">
                                                                                <i class="fa fa-ban"></i>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="isotope-item item grid-sizer"
                                                        style="position: absolute; left: 0px; top: 960px; display: none;">
                                                    </li>
                                                </ul>
                                            </div>



                                        </div>
                                    </div>
                                    <div id="fieldtabproductsisotope" class="horizontal_mode">
                                        <ul class="fieldtabproductsisotope-filters">
                                            <li class="fieldtabproductsisotope-filter title_font">
                                                <a href="#" data-filter="new-arrivals" class="active">
                                                    <span class="text">Limited offers</span> </a>
                                            </li>
                                        </ul>
                                        <div class="row">
                                            <div class="fieldtabproductsisotope-products">
                                                <ul class="isotope-grid" style="position: relative; height: 640px;">
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals sale-products"
                                                        style="position: absolute; left: 0px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="14" data-id-product-attribute="13"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class=" thumbnail product-thumbnail ">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="https://demo.fieldthemes.com/ps_medicine/home1/79-large_default/integer-cursus-auctor-ex.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                        <div class="conditions-box">
                                                                        </div>
                                                                        <span class="item-countdown">
                                                                            <span class="bg_tranp"></span>
                                                                            <span class="item-countdown-time"
                                                                                data-time="2023-03-25 00:00:00"><span
                                                                                    class="section_cout"><span
                                                                                        class="Days">394
                                                                                    </span><span
                                                                                        class="text">Days</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Hours">18
                                                                                    </span><span
                                                                                        class="text">Hours</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Minutes">49
                                                                                    </span><span
                                                                                        class="text">Mins</span></span><span
                                                                                    class="section_cout"><span
                                                                                        class="Seconds">55
                                                                                    </span><span
                                                                                        class="text">Secs</span></span></span>
                                                                        </span>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 225px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="21" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg" alt=""
                                                                                    style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="21">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 450px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="17" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../81-large_default/quisque-at-orci-gravid-.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>
                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>

                                                                        </a>
                                                                        <!-- <div class="conditions-box">
                                                                            <span class="sale_product">Sale</span>

                                                                        </div> -->
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>

                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="17">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="col-xs-12 col-sm-6 col-md-3 isotope-item item new-arrivals"
                                                        style="position: absolute; left: 675px; top: 0px;">
                                                        <div class="item">
                                                            <div class="item-inner">
                                                                <div class="product-miniature js-product-miniature"
                                                                    data-id-product="18" data-id-product-attribute="0"
                                                                    itemscope="" itemtype="http://schema.org/Product">
                                                                    <div class="left-product">
                                                                        <a href="Propecia.php"
                                                                            class="thumbnail product-thumbnail">
                                                                            <span class="cover_image">
                                                                                <img src="../pills/img/medi2.jpg"
                                                                                    data-full-size-image-url="../82-large_default/aliquam-tincidunt-mauris.jpg"
                                                                                    alt="" style="margin-left: 2.3rem;">
                                                                            </span>

                                                                            <div class="product-description">
                                                                                <div class="product_name"><a
                                                                                        href="Propecia.php">A
                                                                                        tincidunt mauris.</a></div>
                                                                                <div class="product-price-and-shipping">
                                                                                    <span class="price">$30.39</span>

                                                                                </div>
                                                                            </div>
                                                                            <!-- <div class="right-product">
                                                                                </div> -->

                                                                        </a>
                                                                        <div class="conditions-box">

                                                                        </div>
                                                                        <!-- <div class="quick-view-product">
                                                                                <a href="javascript:void(0)"
                                                                                    class="quick-view"
                                                                                    data-link-action="quickview"
                                                                                    title="Quick view">
                                                                                    <i class="fa fa-eye"></i>
                                                                                    Quick view
                                                                                </a>
                                                                            </div> -->
                                                                    </div>
                                                                    <!-- <div class="addtocart">
                                                                        <form
                                                                            action="https://demo.fieldthemes.com/ps_medicine/home1/en/cart"
                                                                            method="post">
                                                                            <input type="hidden" name="token"
                                                                                value="9494207df78e080f0efb7bd612000682">
                                                                            <input type="hidden" name="id_product"
                                                                                value="18">
                                                                            <button class="add-to-cart"
                                                                                data-button-action="add-to-cart"
                                                                                type="submit">
                                                                                <span title="Add to cart"><i
                                                                                        class="fa fa-shopping-cart"></i>
                                                                                    ADD TO CART
                                                                                </span>

                                                                            </button>
                                                                        </form>
                                                                    </div> -->
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li class="isotope-item item grid-sizer"
                                                        style="position: absolute; left: 0px; top: 960px; display: none;">
                                                    </li>
                                                </ul>
                                            </div>



                                        </div>
                                    </div>



                                </div>
                            </div>
                </div>



            </div>
            </div>


        </section>

        <?php
include ('partials/footer.php');
?>

    </main>

    <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>

</body>




</html>