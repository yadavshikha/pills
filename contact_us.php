<!doctype html>
<html lang="en">


<!-- Mirrored from demo.fieldthemes.com/ps_medicine/home1/en/contact-us by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Feb 2022 13:44:58 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<?php
include ('partials/head.php');
?>

<body id="contact"
    class="lang-en country-us currency-usd layout-left-column page-contact tax-display-disabled fullwidth">




    <main>

        <?php
include ('partials/header.php');
?>


        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <!--END SLIDER SHOW -->


        <aside id="notifications">
            <div class="container">



            </div>
        </aside>

        <!-- <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">




                <div class="row">

                    <div id="left-column" class="col-xs-12 col-sm-3">

                        <div class="contact-rich">
                            <h4>Store information</h4>
                            <div class="block">
                                <div class="icon"><i class="material-icons">&#xE55F;</i></div>
                                <div class="data">Medicine home 1<br />United States</div>
                            </div>
                            <hr />
                            <div class="block">
                                <div class="icon"><i class="material-icons">&#xE158;</i></div>
                                <div class="data email">
                                    Email us:<br />
                                </div>
                                <a href="mailto:demo@fieldthemes.com">demo@fieldthemes.com</a>
                            </div>
                        </div>

                    </div>



                    <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

                        <section id="main">
                            <h2 style="display:none">.</h2>




                            <section id="content" class="page-content card card-block">


                                <section class="contact-form">
                                    <form action="https://demo.fieldthemes.com/ps_medicine/home1/en/contact-us"
                                        method="post" enctype="multipart/form-data">


                                        <section class="form-fields">

                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <h3>Contact us</h3>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label">Subject</label>
                                                <div class="col-md-6">
                                                    <select name="id_contact" class="form-control form-control-select">
                                                        <option value="2">Customer service</option>
                                                        <option value="1">Webmaster</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label">Email address</label>
                                                <div class="col-md-6">
                                                    <input class="form-control" name="from" type="email" value=""
                                                        placeholder="your@email.com">
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label">Attachment</label>
                                                <div class="col-md-6">
                                                    <input type="file" name="fileUpload" class="filestyle">
                                                </div>
                                                <span class="col-md-3 form-control-comment">
                                                    optional
                                                </span>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 form-control-label">Message</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="message"
                                                        placeholder="How can we help?" rows="3"></textarea>
                                                </div>
                                            </div>

                                        </section>

                                        <footer class="form-footer text-xs-right">
                                            <input class="btn btn-primary" type="submit" name="submitMessage"
                                                value="Send">
                                        </footer>

                                    </form>
                                </section>


                            </section>



                            

                        </section>


                    </div>



                </div>



            </div>

        </section> -->



        <section id="wrapper" style="margin-bottom: 2rem;">
            <h2 style="display:none">.</h2>
            <div class="container">

                <div id="content-wrapper">

                    <section id="main">
                        <h2 style="display:none">.</h2>

                        <section id="content" class="page-home">

                            <div class="row">
                                <div class="col-xs-12 col-md-2">
                                    <div id="fieldblockcategories" class="block horizontal_mode clearfix">
                                        <div class="text2-border">
                                            <h2 class="title_font">
                                                <a class="title_text">
                                                    Product Categories
                                                </a>
                                            </h2>
                                        </div>
                                        <div class="box_categories">
                                            <div class="row">
                                                <div id="field_content">
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Bestsellers">
                                                            Bestsellers
                                                        </a>
                                                    </div>

                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Erectile Dysfunction">
                                                            Erectile Dysfunction
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="ED Sets">
                                                            ED Sets
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Allergies">
                                                            Allergies
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Anti Fungal">
                                                            Anti Fungal
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Anti Viral">
                                                            Anti Viral
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Antibiotics ">
                                                            Antibiotics
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Anxiety ">
                                                            Anxiety
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Arthritis  ">
                                                            Arthritis
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Asthma">
                                                            Asthma
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Birth Control">
                                                            Birth Control
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Blood Pressure">
                                                            Blood Pressure
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Cholesterol Lowering">
                                                            Cholesterol Lowering
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Depression">
                                                            Depression
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Diabetes">
                                                            Diabetes
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Gastrointestinal">
                                                            Gastrointestinal
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Hair Loss">
                                                            Hair Loss
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Heart Disease">
                                                            Heart Disease
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Herbals">
                                                            Herbals
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Man's Health ">
                                                            Man's Health
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Muscle Relaxant">
                                                            Muscle Relaxant
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Other">
                                                            Other
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title=" Pain Relief">
                                                            Pain Relief
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Skincare">
                                                            Skincare
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Sleep Aid">
                                                            Sleep Aid
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Quit Smoking">
                                                            Quit Smoking
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Weight Loss">
                                                            Weight Loss
                                                        </a>
                                                    </div>
                                                    <div class="item-inner">
                                                        <a class="name_block" href="medicine-category.php"
                                                            title="Woman's Health ">
                                                            Woman's Health
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                </div>
                                <div class="col-xs-12 col-md-10">



                                    <div id="fieldtabproductsisotope" class="horizontal_mode">
                                        <ul class="fieldtabproductsisotope-filters">
                                            <li class="fieldtabproductsisotope-filter title_font">
                                                <a href="#" data-filter="new-arrivals" class="active">
                                                    <span class="text">contact us</span> </a>
                                            </li>

                                        </ul>
                                        <div class="row">

                                            <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

                                                <section id="main">
                                                    <h2 style="display:none">.</h2>




                                                    <section id="content" class="page-content card card-block">


                                                        <section class="contact-form">
                                                            <form
                                                                action="https://demo.fieldthemes.com/ps_medicine/home1/en/contact-us"
                                                                method="post" enctype="multipart/form-data">


                                                                <section class="form-fields">

                                                                    <div class="form-group row">
                                                                        <div class="col-md-12">
                                                                            <h3>Contact us</h3>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label
                                                                            class="col-md-3 form-control-label">Name</label>
                                                                        <div class="col-md-6">
                                                                            <input class="form-control" name="name"
                                                                                type="text" value=""
                                                                                placeholder="Your Name">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-md-3 form-control-label">Email
                                                                            address</label>
                                                                        <div class="col-md-6">
                                                                            <input class="form-control" name="from"
                                                                                type="email" value=""
                                                                                placeholder="your@email.com">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label
                                                                            class="col-md-3 form-control-label">Subject</label>
                                                                        <div class="col-md-6">
                                                                            <input class="form-control" name="subject"
                                                                                type="text" value="">
                                                                        </div>
                                                                    </div>


                                                                  
                                                                    <div class="form-group row">
                                                                        <label
                                                                            class="col-md-3 form-control-label">Message</label>
                                                                        <div class="col-md-9">
                                                                            <textarea class="form-control"
                                                                                name="message"
                                                                                placeholder="How can we help?"
                                                                                rows="3"></textarea>
                                                                        </div>
                                                                    </div>

                                                                </section>

                                                                <footer class="form-footer text-xs-right">
                                                                    <input class="btn btn-primary" type="submit"
                                                                        name="submitMessage" value="Send">
                                                                </footer>

                                                            </form>
                                                        </section>


                                                    </section>





                                                </section>


                                            </div>



                                        </div>
                                    </div>



                                </div>
                            </div>
                </div>



            </div>
            </div>


        </section>

        <?php
include ('partials/footer.php');
?>

    </main>


    <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>


   

</body>




</html>