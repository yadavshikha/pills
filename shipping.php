<!doctype html>
<html lang="en">
<?php
include ('partials/head.php');
?>

<body id="checkout" class="lang-en country-us currency-usd layout-full-width page-order tax-display-disabled fullwidth">




    <main>


        <?php
include ('partials/header.php');
?>
        <!--END MEGAMENU -->
        <!-- SLIDER SHOW -->
        <!--END SLIDER SHOW -->


        <aside id="notifications">
            <div class="container">
            </div>
        </aside>
        <section id="wrapper">
            <h2 style="display:none">.</h2>
            <div class="container">
                <section id="content">
                    <div class="row">
                        <div class="col-md-8">
                            <section id="checkout-personal-information-step" class="checkout-step -reachable -complete">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">1</span>
                                    Personal Information
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>

                                <div class="content">


                                    <ul class="nav nav-inline m-y-2" style="align-items: center;">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#checkout-guest-form"
                                                role="tab">
                                                Order as a guest
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <span href="nav-separator"> | </span>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link " data-link-action="show-login-form" data-toggle="tab"
                                                href="#checkout-login-form" role="tab">
                                                Sign in
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="checkout-guest-form" role="tabpanel">
                                            <form action="" id="customer-form" class="js-customer-form" method="post">
                                                <section>
                                                    <input type="hidden" name="id_customer" value="112">
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label">
                                                            Social title
                                                        </label>
                                                        <div class="col-md-6 form-control-valign">
                                                            <label class="radio-inline">
                                                                <span class="custom-radio">
                                                                    <input name="id_gender" type="radio" value="1"
                                                                        checked>
                                                                    <span></span>
                                                                </span>
                                                                Mr.
                                                            </label>
                                                            <label class="radio-inline">
                                                                <span class="custom-radio">
                                                                    <input name="id_gender" type="radio" value="2">
                                                                    <span></span>
                                                                </span>
                                                                Mrs.
                                                            </label>
                                                        </div>

                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label required">
                                                            First name
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input class="form-control" name="firstname" type="text"
                                                                value="dsaa" required>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label required">
                                                            Last name
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input class="form-control" name="lastname" type="text"
                                                                value="asdsad" required>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label required">
                                                            Email
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input class="form-control" name="email" type="email"
                                                                value="sadswgd@gmail.com" required>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                    <p>
                                                        <span class="font-weight-bold">Create an account</span> <span
                                                            class="font-italic">(optional)</span>
                                                        <br>
                                                        <span class="text-muted">And save time on your next
                                                            order!</span>
                                                    </p>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label">
                                                            Password
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="input-group js-parent-focus">
                                                                <input
                                                                    class="form-control js-child-focus js-visible-password"
                                                                    name="password" type="password" value=""
                                                                    pattern=".{5,}">
                                                                <span class="input-group-btn">
                                                                    <button class="btn" type="button"
                                                                        data-action="show-password"
                                                                        data-text-show="Show" data-text-hide="Hide">
                                                                        Show
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                            Optional
                                                        </div>
                                                    </div>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label">
                                                            Birthdate
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input class="form-control" name="birthday" type="text"
                                                                value="" placeholder="MM/DD/YYYY">
                                                            <span class="form-control-comment">
                                                                (E.g.: 05/31/1970)
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                            Optional
                                                        </div>
                                                    </div>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label">
                                                        </label>
                                                        <div class="col-md-6">
                                                            <span class="custom-checkbox">
                                                                <input name="optin" type="checkbox" value="1">
                                                                <span><i
                                                                        class="material-icons checkbox-checked">&#xE5CA;</i></span>
                                                                <label>Receive offers from our partners</label>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label">
                                                        </label>
                                                        <div class="col-md-6">
                                                            <span class="custom-checkbox">
                                                                <input name="newsletter" type="checkbox" value="1"
                                                                    checked="checked">
                                                                <span><i
                                                                        class="material-icons checkbox-checked">&#xE5CA;</i></span>
                                                                <label>Sign up for our newsletter<br><em>You may
                                                                        unsubscribe at any moment. For that purpose,
                                                                        please find our contact info in the legal
                                                                        notice.</em></label>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                </section>
                                                <footer class="form-footer clearfix">
                                                    <input type="hidden" name="submitCreate" value="1">
                                                    <button class="continue btn btn-primary pull-xs-right"
                                                        name="continue" data-link-action="register-new-customer"
                                                        type="submit" value="1">
                                                        Continue
                                                    </button>
                                                </footer>
                                            </form>
                                        </div>
                                        <div class="tab-pane " id="checkout-login-form" role="tabpanel">
                                            <form id="login-form" action="#" method="post">
                                                <section>
                                                    <input type="hidden" name="back" value="">
                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label required">
                                                            Email
                                                        </label>
                                                        <div class="col-md-6">
                                                            <input class="form-control" name="email" type="email"
                                                                value="" required>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row ">
                                                        <label class="col-md-3 form-control-label required">
                                                            Password
                                                        </label>
                                                        <div class="col-md-6">
                                                            <div class="input-group js-parent-focus">
                                                                <input
                                                                    class="form-control js-child-focus js-visible-password"
                                                                    name="password" type="password" value=""
                                                                    pattern=".{5,}" required>
                                                                <span class="input-group-btn">
                                                                    <button class="btn" type="button"
                                                                        data-action="show-password"
                                                                        data-text-show="Show" data-text-hide="Hide">
                                                                        Show
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 form-control-comment">
                                                        </div>
                                                    </div>
                                                    <div class="forgot-password">
                                                        <a href="" rel="nofollow">
                                                            Forgot your password?
                                                        </a>
                                                    </div>
                                                </section>
                                                <footer class="form-footer text-xs-center clearfix">
                                                    <input type="hidden" name="submitLogin" value="1">

                                                    <button class="continue btn btn-primary pull-xs-right"
                                                        name="continue" data-link-action="sign-in" type="submit"
                                                        value="1">
                                                        Continue
                                                    </button>
                                                </footer>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- <section id="checkout-addresses-step" class="checkout-step -reachable -complete">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">2</span>
                                    Addresses
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>
                                <div class="content">

                                    <div class="js-address-form">
                                        <form method="POST" action="#" data-refresh-url="#?ajax=1&action=addressForm">
                                            <p>
                                                The selected address will be used both as your personal address (for
                                                invoice) and as your delivery address.
                                            </p>

                                            <div id="delivery-addresses" class="address-selector js-address-selector">
                                                <article class="address-item selected"
                                                    id="id-address-delivery-address-72">
                                                    <header class="h4">
                                                        <label class="radio-block">
                                                            <span class="custom-radio">
                                                                <input type="radio" name="id_address_delivery"
                                                                    value="72" checked>
                                                                <span></span>
                                                            </span>
                                                            <span class="address-alias h4">My Address</span>
                                                            <div class="address">dsaa asdsad<br>fdfd<br>fddvfd
                                                                dfdfsd<br>mumbai, Maine 04005<br>United States</div>
                                                        </label>
                                                    </header>
                                                    <hr>
                                                    <footer class="address-footer">
                                                        <a class="edit-address text-muted"
                                                            data-link-action="edit-address"
                                                            href="#?id_address=72&editAddress=delivery&token=16f4dfcf680a6f04a38f4b4b093bc8fe">
                                                            <i class="material-icons edit">&#xE254;</i>Edit
                                                        </a>
                                                        <a class="delete-address text-muted"
                                                            data-link-action="delete-address"
                                                            href="#?id_address=72&deleteAddress=1&token=16f4dfcf680a6f04a38f4b4b093bc8fe">
                                                            <i class="material-icons delete">&#xE872;</i>Delete
                                                        </a>
                                                    </footer>
                                                </article>
                                                <p>
                                                    <button class="ps-hidden-by-js form-control-submit center-block"
                                                        type="submit">Save</button>
                                                </p>
                                            </div>

                                            <p class="add-address">
                                                <a href="#?newAddress=delivery"><i
                                                        class="material-icons">&#xE145;</i>add new address</a>
                                            </p>

                                            <p>
                                                <a data-link-action="different-invoice-address"
                                                    href="#?use_same_address=0">
                                                    Billing address differs from shipping address
                                                </a>
                                            </p>



                                            <div class="clearfix">
                                                <button type="submit" class="btn btn-primary continue pull-xs-right"
                                                    name="confirm-addresses" value="1">
                                                    Continue
                                                </button>
                                            </div>

                                        </form>
                                    </div>

                                </div>
                            </section> -->
                            <section id="checkout-addresses-step"
                                class="checkout-step -current -reachable js-current-step">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">2</span>
                                    Addresses
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>

                                <div class="content">

                                    <div class="js-address-form">
                                        <form method="POST" action=""
                                            data-refresh-url="https://demo.fieldthemes.com/ps_medicine/home1/en/order?ajax=1&action=addressForm">
                                            <p>
                                                The selected address will be used both as your personal address (for
                                                invoice) and as your delivery address.
                                            </p>
                                            <div id="delivery-address">
                                                <div class="js-address-form">
                                                    <form method="POST"
                                                        action="https://demo.fieldthemes.com/ps_medicine/home1/en/address?id_address=0"
                                                        data-id-address="0"
                                                        data-refresh-url="https://demo.fieldthemes.com/ps_medicine/home1/en/address?ajax=1&action=addressForm">
                                                        <section class="form-fields">
                                                            <input type="hidden" name="id_address" value="">
                                                            <input type="hidden" name="id_customer" value="">
                                                            <input type="hidden" name="back" value="">
                                                            <input type="hidden" name="token"
                                                                value="fdd6f38386170833744426278d07365e">
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    First name
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="firstname"
                                                                        type="text" value="dsaa" maxlength="32"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Last name
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="lastname"
                                                                        type="text" value="asdsad" maxlength="32"
                                                                        required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label">
                                                                    Company
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="company"
                                                                        type="text" value="" maxlength="64">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                    Optional
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Address
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="address1"
                                                                        type="text" value="" maxlength="128" required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label">
                                                                    Address Complement
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="address2"
                                                                        type="text" value="" maxlength="128">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                    Optional
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    City
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="city" type="text"
                                                                        value="" maxlength="64" required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    State
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <select class="form-control form-control-select"
                                                                        name="id_state" required>
                                                                        <option value disabled selected>-- please choose
                                                                            --</option>
                                                                        <option value="1">AA</option>
                                                                        <option value="2">AE</option>
                                                                        <option value="3">AP</option>
                                                                        <option value="4">Alabama</option>
                                                                        <option value="5">Alaska</option>
                                                                        <option value="6">Arizona</option>
                                                                        <option value="7">Arkansas</option>
                                                                        <option value="8">California</option>
                                                                        <option value="9">Colorado</option>
                                                                        <option value="10">Connecticut</option>
                                                                        <option value="11">Delaware</option>
                                                                        <option value="12">Florida</option>
                                                                        <option value="13">Georgia</option>
                                                                        <option value="14">Hawaii</option>
                                                                        <option value="15">Idaho</option>
                                                                        <option value="16">Illinois</option>
                                                                        <option value="17">Indiana</option>
                                                                        <option value="18">Iowa</option>
                                                                        <option value="19">Kansas</option>
                                                                        <option value="20">Kentucky</option>
                                                                        <option value="21">Louisiana</option>
                                                                        <option value="22">Maine</option>
                                                                        <option value="23">Maryland</option>
                                                                        <option value="24">Massachusetts</option>
                                                                        <option value="25">Michigan</option>
                                                                        <option value="26">Minnesota</option>
                                                                        <option value="27">Mississippi</option>
                                                                        <option value="28">Missouri</option>
                                                                        <option value="29">Montana</option>
                                                                        <option value="30">Nebraska</option>
                                                                        <option value="31">Nevada</option>
                                                                        <option value="32">New Hampshire</option>
                                                                        <option value="33">New Jersey</option>
                                                                        <option value="34">New Mexico</option>
                                                                        <option value="35">New York</option>
                                                                        <option value="36">North Carolina</option>
                                                                        <option value="37">North Dakota</option>
                                                                        <option value="38">Ohio</option>
                                                                        <option value="39">Oklahoma</option>
                                                                        <option value="40">Oregon</option>
                                                                        <option value="41">Pennsylvania</option>
                                                                        <option value="42">Rhode Island</option>
                                                                        <option value="43">South Carolina</option>
                                                                        <option value="44">South Dakota</option>
                                                                        <option value="45">Tennessee</option>
                                                                        <option value="46">Texas</option>
                                                                        <option value="47">Utah</option>
                                                                        <option value="48">Vermont</option>
                                                                        <option value="49">Virginia</option>
                                                                        <option value="50">Washington</option>
                                                                        <option value="51">West Virginia</option>
                                                                        <option value="52">Wisconsin</option>
                                                                        <option value="53">Wyoming</option>
                                                                        <option value="54">Puerto Rico</option>
                                                                        <option value="55">US Virgin Islands</option>
                                                                        <option value="56">District of Columbia</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Zip/Postal Code
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="postcode"
                                                                        type="text" value="" maxlength="12" required>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">
                                                                    Country
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <select
                                                                        class="form-control form-control-select js-country"
                                                                        name="id_country" required>
                                                                        <option value disabled selected>-- please choose
                                                                            --</option>
                                                                        <option value="21" selected>United States
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label">
                                                                    Phone
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <input class="form-control" name="phone" type="text"
                                                                        value="" maxlength="32">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                    Optional
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="saveAddress" value="delivery">
                                                            <div class="form-group row">
                                                                <div class="col-md-9 col-md-offset-3">
                                                                    <input name="use_same_address" type="checkbox"
                                                                        value="1" checked>
                                                                    <label>Use this address for invoice too</label>
                                                                </div>
                                                            </div>
                                                        </section>
                                                        <footer class="form-footer clearfix">
                                                            <input type="hidden" name="submitAddress" value="1">

                                                            <form>
                                                                <button type="submit"
                                                                    class="continue btn btn-primary pull-xs-right"
                                                                    name="confirm-addresses" value="1">
                                                                    Continue
                                                                </button>
                                                            </form>

                                                        </footer>
                                                    </form>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>

                            <section id="checkout-delivery-step"
                                class="checkout-step -current -reachable js-current-step">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">3</span>
                                    Age verification
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>

                                <div class="content">

                                    <div id="hook-display-before-carrier">

                                    </div>

                                    <div class="delivery-options-list">
                                        <form class="clearfix" id="js-delivery"
                                            data-url-update="#?ajax=1&action=selectDeliveryOption" method="post">
                                            <div class="form-fields">

                                                <div class="form-group">
                                                    <label class="form-group-title lb-14 text-lightbold">Date of
                                                        birth:</label>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-8 col-lg-7">
                                                            <div class="row row-select-inline">
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <div class="select-wrapper">
                                                                        <select name="birth_day" id="birth_day"
                                                                            class="tx tx-big flex-xs" tabindex="1">
                                                                            <option value="">Day</option>
                                                                            <option value="01">01</option>
                                                                            <option value="02">02</option>
                                                                            <option value="03">03</option>
                                                                            <option value="04">04</option>
                                                                            <option value="05">05</option>
                                                                            <option value="06">06</option>
                                                                            <option value="07">07</option>
                                                                            <option value="08">08</option>
                                                                            <option value="09">09</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>

                                                                        </select>
                                                                    </div>
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <div class="select-wrapper">
                                                                        <select name="birth_month" id="birth_month"
                                                                            class="tx tx-big flex-xs" tabindex="1">
                                                                            <option value="">Month</option>
                                                                            <option value="01">01</option>
                                                                            <option value="02">02</option>
                                                                            <option value="03">03</option>
                                                                            <option value="04">04</option>
                                                                            <option value="05">05</option>
                                                                            <option value="06">06</option>
                                                                            <option value="07">07</option>
                                                                            <option value="08">08</option>
                                                                            <option value="09">09</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>

                                                                        </select>
                                                                    </div>
                                                                    &nbsp;
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <div class="select-wrapper">
                                                                        <select name="birth_year" id="birth_year"
                                                                            class="tx tx-big flex-xs" tabindex="1">
                                                                            <option value="">Year</option>
                                                                            <option value="2004">2004</option>
                                                                            <option value="2003">2003</option>
                                                                            <option value="2002">2002</option>
                                                                            <option value="2001">2001</option>
                                                                            <option value="2000">2000</option>
                                                                            <option value="1999">1999</option>
                                                                            <option value="1998">1998</option>
                                                                            <option value="1997">1997</option>
                                                                            <option value="1996">1996</option>
                                                                            <option value="1995">1995</option>
                                                                            <option value="1994">1994</option>
                                                                            <option value="1993">1993</option>
                                                                            <option value="1992">1992</option>
                                                                            <option value="1991">1991</option>
                                                                            <option value="1990">1990</option>
                                                                            <option value="1989">1989</option>
                                                                            <option value="1988">1988</option>
                                                                            <option value="1987">1987</option>
                                                                            <option value="1986">1986</option>
                                                                            <option value="1985">1985</option>
                                                                            <option value="1984">1984</option>
                                                                            <option value="1983">1983</option>
                                                                            <option value="1982">1982</option>
                                                                            <option value="1981">1981</option>
                                                                            <option value="1980">1980</option>
                                                                            <option value="1979">1979</option>
                                                                            <option value="1978">1978</option>
                                                                            <option value="1977">1977</option>
                                                                            <option value="1976">1976</option>
                                                                            <option value="1975">1975</option>
                                                                            <option value="1974">1974</option>
                                                                            <option value="1973">1973</option>
                                                                            <option value="1972">1972</option>
                                                                            <option value="1971">1971</option>
                                                                            <option value="1970">1970</option>
                                                                            <option value="1969">1969</option>
                                                                            <option value="1968">1968</option>
                                                                            <option value="1967">1967</option>
                                                                            <option value="1966">1966</option>
                                                                            <option value="1965">1965</option>
                                                                            <option value="1964">1964</option>
                                                                            <option value="1963">1963</option>
                                                                            <option value="1962">1962</option>
                                                                            <option value="1961">1961</option>
                                                                            <option value="1960">1960</option>
                                                                            <option value="1959">1959</option>
                                                                            <option value="1958">1958</option>
                                                                            <option value="1957">1957</option>
                                                                            <option value="1956">1956</option>
                                                                            <option value="1955">1955</option>
                                                                            <option value="1954">1954</option>
                                                                            <option value="1953">1953</option>
                                                                            <option value="1952">1952</option>
                                                                            <option value="1951">1951</option>
                                                                            <option value="1950">1950</option>
                                                                            <option value="1949">1949</option>
                                                                            <option value="1948">1948</option>
                                                                            <option value="1947">1947</option>
                                                                            <option value="1946">1946</option>
                                                                            <option value="1945">1945</option>
                                                                            <option value="1944">1944</option>
                                                                            <option value="1943">1943</option>
                                                                            <option value="1942">1942</option>
                                                                            <option value="1941">1941</option>
                                                                            <option value="1940">1940</option>
                                                                            <option value="1939">1939</option>
                                                                            <option value="1938">1938</option>
                                                                            <option value="1937">1937</option>
                                                                            <option value="1936">1936</option>
                                                                            <option value="1935">1935</option>
                                                                            <option value="1934">1934</option>
                                                                            <option value="1933">1933</option>
                                                                            <option value="1932">1932</option>
                                                                            <option value="1931">1931</option>
                                                                            <option value="1930">1930</option>
                                                                            <option value="1929">1929</option>
                                                                            <option value="1928">1928</option>
                                                                            <option value="1927">1927</option>
                                                                            <option value="1926">1926</option>
                                                                            <option value="1925">1925</option>
                                                                            <option value="1924">1924</option>
                                                                            <option value="1923">1923</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <label for="birthDate" class="form-group-message lb-13"
                                                                style="display: none;">This field is required.</label>
                                                        </div>

                                                    </div>


                                                </div>

                                                <div class="order-options">
                                                </div>
                                            </div>
                                            <footer class="form-footer clearfix">
                                                <input type="hidden" name="submitCreate" value="1">
                                                <button class="continue btn btn-primary pull-xs-right" name="continue"
                                                    data-link-action="register-new-customer" type="submit" value="1">
                                                    Continue
                                                </button>
                                            </footer>
                                        </form>
                                    </div>

                                    <div id="hook-display-after-carrier">

                                    </div>

                                    <div id="extra_carrier"></div>

                                </div>
                            </section>

                            <section id="checkout-delivery-step"
                                class="checkout-step -current -reachable js-current-step">
                                <h1 class="step-title h3">
                                    <i class="material-icons done">&#xE876;</i>
                                    <span class="step-number">4</span>
                                    Shipping Method
                                    <span class="step-edit text-muted"><i class="material-icons edit">mode_edit</i>
                                        edit</span>
                                </h1>

                                <div class="content">

                                    <div id="hook-display-before-carrier">

                                    </div>

                                    <div class="delivery-options-list">
                                        <form class="clearfix" id="js-delivery"
                                            data-url-update="#?ajax=1&action=selectDeliveryOption" method="post">
                                            <div class="form-fields">

                                                <div class="delivery-options">
                                                    <div class="delivery-option">
                                                        <div class="col-sm-1">
                                                            <span class="custom-radio pull-xs-left">
                                                                <input type="radio" name="delivery_option[72]"
                                                                    id="delivery_option_2" value="2," checked>
                                                                <span></span>
                                                            </span>
                                                        </div>
                                                        <label for="delivery_option_2"
                                                            class="col-sm-11 delivery-option-2">
                                                            <div class="row">
                                                                <div class="col-sm-5 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-xs-3">
                                                                            <img src="/ps_medicine/home1/img/s/2.jpg"
                                                                                alt="My carrier" />
                                                                        </div>
                                                                        <div class="col-xs-9">
                                                                            <span class="h6 carrier-name">My
                                                                                carrier</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4 col-xs-12">
                                                                    <span class="carrier-delay">Delivery next
                                                                        day!</span>
                                                                </div>
                                                                <div class="col-sm-3 col-xs-12">
                                                                    <span class="carrier-price">$7.00 tax excl.</span>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        <div class="col-md-12 carrier-extra-content">

                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="order-options">
                                                </div>
                                            </div>
                                            <div class="checkout cart-detailed-actions card-block">
                                                <div class="text-xs-center">
                                                    <a href="order_confirmation.php"
                                                        class="btn ">CONTINUE</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div id="hook-display-after-carrier">

                                    </div>

                                    <div id="extra_carrier"></div>

                                </div>
                            </section>







                            
                            

                            <!-- <section class="checkout-step -unreachable" id="checkout-payment-step">
                                <h1 class="step-title h3">
                                    <span class="step-number">4</span> Payment
                                </h1>
                            </section> -->
                        </div>
                        <div class="cart-grid-right col-xs-12 col-lg-4">
                            <div class="card cart-summary">
                                <div class="cart-detailed-totals">
                                    <div class="card-block">
                                        <div class="cart-summary-line" id="cart-subtotal-products">
                                            <span class="label js-subtotal">
                                                0 items
                                            </span>
                                            <span class="value">$0.00</span>
                                        </div>
                                        <div class="cart-summary-line" id="cart-subtotal-shipping">
                                            <span class="label">
                                                Shipping
                                            </span>
                                            <span class="value">Free</span>
                                            <div><small class="value"></small></div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="card-block">
                                        <div class="cart-summary-line cart-total">
                                            <span class="label">Total (tax excl.)</span>
                                            <span class="value">$0.00</span>
                                        </div>
                                        <div class="cart-summary-line">
                                            <small class="label">Taxes</small>
                                            <small class="value">$0.00</small>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <!-- <div class="checkout text-xs-center card-block"> -->
                                <!-- <button type="button" class="btn btn-primary disabled"
                                            disabled>Checkout</button> -->
                                <!-- <button type="button" class="btn btn-primary ">Checkout</button>
                                    </div> -->
                            </div>
                            <div id="block-reassurance">
                                <ul>
                                    <li>
                                        <div class="block-reassurance-item">
                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png"
                                                alt="Security policy (edit with module Customer reassurance)">
                                            <span class="h6">Security policy (edit with module Customer
                                                reassurance)</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block-reassurance-item">
                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png"
                                                alt="Delivery policy (edit with module Customer reassurance)">
                                            <span class="h6">Delivery policy (edit with module Customer
                                                reassurance)</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block-reassurance-item">
                                            <img src="https://demo.fieldthemes.com/ps_medicine/home1/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png"
                                                alt="Return policy (edit with module Customer reassurance)">
                                            <span class="h6">Return policy (edit with module Customer
                                                reassurance)</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </section>
        <?php
include ('partials/footer.php');
?>



    </main>

    <!-- <script type="text/javascript"
        src="assets/js/bottom-1d7c42.js">
    </script> -->

    <script type="text/javascript" src="assets/js/bottom-d5a762.js"></script>



</body>

</html>